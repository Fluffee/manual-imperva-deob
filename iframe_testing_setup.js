//List of copy pastable commands to get some usable iframe vars that way commands from the imperava fingerprinter can be ran to analyze the results (and ensure the deob correctly produces results)

var documentReference = window["document"];
var findWhitespaceRegex = /\s/g;

var tridentRegex = /Trident/;
var unsafeCharRegex = new window.RegExp("[\\u007F-\\uFFFF]", "g");
function randomNumberGeneratorFactory(firstSeed, secondSeed) {
    var firstSeedRef = firstSeed;
    var secondSeedRef = secondSeed;
    return function () {
        var startingSecondSeed = secondSeedRef;
        var newSecondSeed = firstSeedRef;
        firstSeedRef = startingSecondSeed;

        newSecondSeed ^= newSecondSeed << 23;
        newSecondSeed ^= newSecondSeed >> 17;
        newSecondSeed ^= startingSecondSeed;
        newSecondSeed ^= startingSecondSeed >> 26;
        
        secondSeedRef = newSecondSeed;
        return (firstSeedRef + secondSeedRef) % 4294967296;
    };
}

function convertFirstCharToUnicode(word) {
    return "\\u" + ("0000" + word.charCodeAt(0).toString(16)).substr(-4);
}

function jsonStringUndefinedReplace(key, value) {
    return value === undefined ? null : value;
};

function isFunctionNative(someFunction) {
    if (typeof(someFunction) === "function" ) {
        let functionCode = someFunction["toString"]();
        let cleanedFunctionCode = functionCode["replace"](findWhitespaceRegex, "");
        return endsWith(cleanedFunctionCode, "{[nativecode]}");
    }

    return false;
}

function performDoubleShift(incoming, shift1, shift2) {      
    return performSingleArrayShift(performSingleArrayShift(incoming, shift1), shift2);
}

function performSingleArrayShift(incoming, shift) {
    let outgoing = [];
    for (let i = 0 ; i < incoming.length; i++) {
      outgoing.push(incoming[(i + shift) % incoming.length]);
    }

    return outgoing;
}

function swapEverySecondElement(array) {
    let outgoing = new Array(array.length);
    for (let i = 0; i < array.length; i += 2) {
        if (i + 1 >= array.length) {
            outgoing[i] = array[i];
            break;
        }
        outgoing[i] = array[i+1];
        outgoing[i+1] = array[i];
    }

    return outgoing;
}

var iframe = documentReference["createElement"]("IFRAME");
iframe["style"]["display"] = "none";

documentReference["body"]["insertBefore_e421bb29"] = documentReference["body"]["__proto__"]["insertBefore"];
documentReference["body"]["insertBefore_e421bb29"](iframe, documentReference["body"]["firstChild"]);

var randomInteger = window["Math"]["random"]() * 1073741824 | 0;
var iframeContentWindow = iframe["contentWindow"];
var iframeContentNavigator = iframeContentWindow["navigator"];
var iframeContentDocument = iframe["contentDocument"];

var canvasInfoObject = {};
var canvasElement = documentReference["createElement"]("canvas");
canvasElement["width"] = 600;
canvasElement["height"] = 160;
canvasElement["style"]["display"] = "inline";
var canvas2dContext = canvasElement["getContext"]("2d");