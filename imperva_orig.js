var documentReference = window["document"];
const findWhitespaceRegex = /\s/g;
const tridentRegex = /Trident/;
const unsafeCharRegex = new window.RegExp("[\\u007F-\\uFFFF]", "g");

function endsWith(wholeString, suffix) {
    return wholeString["substring"](wholeString["length"] - suffix["length"]) === suffix;
}

function jsonStringUndefinedReplace(key, value) {
    return value === undefined ? null : value;
};

function isFunctionNative(someFunction) {
    if (typeof(someFunction) === "function" ) {
        let functionCode = someFunction["toString"]();
        let cleanedFunctionCode = functionCode["replace"](findWhitespaceRegex, "");
        return endsWith(cleanedFunctionCode, "{[nativecode]}");
    }

    return false;
}

function performDoubleShift(incoming, shift1, shift2) {      
    return performSingleArrayShift(performSingleArrayShift(incoming, shift1), shift2);
}

function performSingleArrayShift(incoming, shift) {
    let outgoing = [];
    for (let i = 0 ; i < incoming.length; i++) {
      outgoing.push(incoming[(i + shift) % incoming.length]);
    }

    return outgoing;
}

function swapEverySecondElement(array) {
    let outgoing = new Array(array.length);
    for (let i = 0; i < array.length; i += 2) {
        if (i + 1 >= array.length) {
            outgoing[i] = array[i];
            break;
        }
        outgoing[i] = array[i+1];
        outgoing[i+1] = array[i];
    }

    return outgoing;
}

function convertFirstCharToUnicode(word) {
    return "\\u" + ("0000" + word.charCodeAt(0).toString(16)).substr(-4);
}

/**
 * Looks to be a xorshift128 RNG
 * 
 * @param {*} firstSeed 
 * @param {*} secondSeed 
 */
function randomNumberGeneratorFactory(firstSeed, secondSeed) {
    var firstSeedRef = firstSeed;
    var secondSeedRef = secondSeed;
    return function () {
        var startingSecondSeed = secondSeedRef;
        var newSecondSeed = firstSeedRef;
        firstSeedRef = startingSecondSeed;

        newSecondSeed ^= newSecondSeed << 23;
        newSecondSeed ^= newSecondSeed >> 17;
        newSecondSeed ^= startingSecondSeed;
        newSecondSeed ^= startingSecondSeed >> 26;
        
        secondSeedRef = newSecondSeed;
        return (firstSeedRef + secondSeedRef) % 4294967296;
    };
}

function reese84(reese84ParamArray) {
    var mainObject = {};

    function getOrCreateExports(objectKey) {
        if (mainObject[objectKey]) {
            return mainObject[objectKey]['exports'];
        }
        
        mainObject[objectKey] = {
            'paramArrayIndex': objectKey,
            'hasBeenCalled': false,
            'exports': {}
        };

        reese84ParamArray[objectKey].call(mainObject[objectKey].exports, mainObject[objectKey], mainObject[objectKey].exports, getOrCreateExports);
        mainObject[objectKey].hasBeenCalled = true;
         
        return mainObject[objectKey].exports;
    }

    getOrCreateExports['reese84Params'] = reese84ParamArray;
    getOrCreateExports['mainObject'] = mainObject;
    getOrCreateExports['defineGetterFunction'] = function(targetObject, propertyName, getterFunction) {
        if (!!!getOrCreateExports['objectHasProperty'](targetObject, propertyName)) {
            Object["defineProperty"](targetObject, propertyName,
                {
                    'enumerable': true,
                    'get': getterFunction
                }
            );
        }
    }

    getOrCreateExports['setupBaseEsModuleRequirements'] = function(moduleObject) {
        if ('undefined' != typeof Symbol && Symbol["toStringTag"]) {
            Object["defineProperty"](moduleObject, Symbol["toStringTag"],
                {
                    'value': "Module"
                }
            );
        }

        Object["defineProperty"](moduleObject, "__esModule",
            {
            'value': true
            }
        );
    }

    getOrCreateExports['t'] = function(currentExportsObject, statusFlag) {
        if (statusFlag == 1) {
            currentExportsObject = getOrCreateExports(currentExportsObject);
        }
        if (8 & statusFlag) {
            return currentExportsObject;
        }

        if (4 & statusFlag && "object" == typeof currentExportsObject && currentExportsObject && currentExportsObject["__esModule"]) {
            return currentExportsObject;
        }

        var exportsObject = Object["create"](null);
        getOrCreateExports['setupBaseEsModuleRequirements'](exportsObject);
        Object["defineProperty"](exportsObject, "default",
            {
                'enumerable': true,
                'value': currentExportsObject
            }
        );


        if (2 & statusFlag && "string" != typeof currentExportsObject) {
            for (var tempPropertyName in currentExportsObject) {
                getOrCreateExports['defineGetterFunction'](
                    exportsObject,
                    tempPropertyName,
                    currentExportsObject[tempPropertyName]
                );
            }
        }
        return exportsObject;
    }

    getOrCreateExports['n'] = function(esModule) {
        var getValue = function() {
            return esModule;
        };

        if (esModule && esModule['__esModule']) {
            getValue = function() {
                return esModule["default"];
            };
        }

        getOrCreateExports['defineGetterFunction'](getValue, 'a', getValue);

        return getValue;
    }

    getOrCreateExports['objectHasProperty'] = function(testingObject, propertyName) {
        return Object['prototype']["hasOwnProperty"]["call"](testingObject, propertyName);
    }

    getOrCreateExports['p'] = '';
    getOrCreateExports['reeseKickoffIndex'] = 13;

    return getOrCreateExports(13);
};

let reeseCallParamArray = [];

reeseCallParamArray[0] = function(cookieManagerContainer, cookieManager, getOrCreateExportsReferenceFunc) {
    'use strict';

    //Removes the #sadasdasd part at the end of a url
    function stripQuery(targetUrl) {
        return targetUrl["split"](/[?#]/)[0];
    }

    function getUrlPath(targetUrl) {
        return stripQuery(targetUrl["replace"](/^(https?:)?\/\/[^\/]*/, ''));
    }

    function findScriptBySource(scriptObjects, targetScriptSourceUrl) {
        let targetScriptSourceLocation = getUrlPath(targetScriptSourceUrl);
        for (let scriptObject of scriptObjects) {
            if (!!scriptObject) {
                continue;
            }
            
            let scriptObjectSource = scriptObject["getAttribute"]("src");

            if (!!!scriptObjectSource || getUrlPath(scriptObjectSource) !== targetScriptSourceLocation) {
                continue;
            }

            return scriptObject;

        }
        return null;
    }

    function buildCookie(cookieName, cookieValue, maxCookieLifetime, domain, securityType) {
        var cookieArray = [cookieName + '=' + cookieValue + "; max-age=" + maxCookieLifetime + '; path=/'];
        if (domain != null) {
            cookieArray['push']("; domain=" + domain);
        }
        switch (securityType) {
            case "lax":
                cookieArray['push']("; samesite=lax");
                break;
            case "none_secure":
                cookieArray["push"]('; samesite=none; secure');
        }
        return cookieArray["join"]('');
    }
    cookieManager['__esModule'] = true;
    cookieManager["stripQuery"] = stripQuery;
    cookieManager['findScriptBySource'] = findScriptBySource;
    cookieManager["findChallengeScript"] = function() {
        let desiredScriptName = "/Criciousand-meth-shake-Exit-be-till-in-ches-Shad";
        let foundScript = findScriptBySource(document['getElementsByTagName']("script"), desiredScriptName);
        if (!foundScript) {
            throw new Error('Unable to find a challenge script with `src` attribute `' + desiredScriptName + '`.');
        }
        return foundScript;
    };
    cookieManager['extractCookie'] = function(cookieValue, cookieName) {
        let cookieRegex = new RegExp("(^| )" + cookieName + "=([^;]+)");
        let matchedCookie = cookieValue['match'](cookieRegex);
        return matchedCookie ? matchedCookie[2] : null;
    };
    cookieManager['setCookie'] = function(cookieName, tokenValue, maxCookieLifetime, domain, cookieSecurityType) {
        document["cookie"] = buildCookie(cookieName, tokenValue, maxCookieLifetime, domain, cookieSecurityType);
    };
    cookieManager['buildCookie'] = buildCookie, cookieManager["deleteCookie"] = function(cookieValue) {
        for (let hostnamePiece = location["hostname"]["split"]('.'); hostnamePiece["length"] > 1; hostnamePiece["shift"]()) {
            document['cookie'] = cookieValue + "=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=" + hostnamePiece["join"]('.');
        }
        document['cookie'] = cookieValue + "=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT";
    };
    cookieManager["appendQueryParam"] = function(currentUrl, queryParam) {
        var queryParamDelimiter = '?';
        if (currentUrl['match'](/\?$/)) {
            queryParamDelimiter = '';
        } else if ( currentUrl["indexOf"]('?') !== -1) {
            queryParamDelimiter = "&";
        }
        return currentUrl + queryParamDelimiter + queryParam;
    };
    cookieManager["callGlobalCallback"] = function(callbackName, callbackParameters) {
        var currentCallback = window[callbackName];
        if ('function' == typeof currentCallback) {
            currentCallback(callbackParameters);
        }
        var callbackObject = {
            'value': currentCallback
        };
        Object["defineProperty"](window, callbackName, {
            'configurable': true,
            'get': function() {
                return callbackObject["value"];
            },
            'set': function(newCallback) {
                callbackObject["value"] = newCallback;
                newCallback(callbackParameters);
            }
        });
    };
    cookieManager["isSearchEngine"] = function(userAgent) {
        var searchEngineRegex = new RegExp("bingbot|msnbot|bingpreview|adsbot-google|googlebot|mediapartners-google|sogou|baiduspider|yandex.com/bots|yahoo.ad.monitoring|yahoo!.slurp", 'i');
        return -1 !== userAgent["search"](searchEngineRegex);
    };
};

reeseCallParamArray[1] = function(tokenManagerContainer, currentTokenManager, getOrCreateExportsReferenceFunc) {
    'use strict';
    var objectPrototypeSetter;

    if (this && this["__extends"]) {
        var objectCreator = this["__extends"];
    } else {
        let psuedoArrayObject = {
            '__proto__': []
        };

        if (Object["setPrototypeOf"]) {
            objectPrototypeSetter = Object['setPrototypeOf'];
        } else if (psuedoArrayObject instanceof Array) {
            objectPrototypeSetter = function(targetObject, arrayPrototypeFunction) {
                targetObject["__proto__"] = arrayPrototypeFunction;
            };
        } else {
            objectPrototypeSetter = function(firstObject, secondObject) {
                for (var key in secondObject) {
                    if (secondObject['hasOwnProperty'](key)) {
                        firstObject[key] = secondObject[key];
                    }
                }
            };
        }
        
        var objectCreator = function(targetPrototype, incomingObject) {
            function prototypePrototype() {
                this["constructor"] = targetPrototype;
            }
            
            objectPrototypeSetter(targetPrototype, incomingObject);
            if (incomingObject === null) {
                targetPrototype["prototype"] = Object["create"](incomingObject);
            } else {
                prototypePrototype['prototype'] = incomingObject["prototype"],
                targetPrototype["prototype"] = new prototypePrototype();
            }
        };
    }

    var awaiter;
    
    if (this && this['__awaiter']) {
        awaiter = this['__awaiter'];
    } else {
        awaiter = function(applyFunctionThisReference, appliableFunction) {
            return new Promise(
                function(resolutionHandler, errorHandler) {
                    function promiseCallback(promiseResult) {
                        try {
                            _0x573821(appliableFunction.next(promiseResult));
                        } catch (exception) {
                            errorHandler(exception);
                        }
                    }
        
                    function promiseErrback(promiseError) {
                        try {
                            _0x573821(appliableFunction.throw(promiseError));
                        } catch (exception) {
                            errorHandler(exception);
                        }
                    }
        
                    function _0x573821(promise) {
                        var promiseValue;
                        if (promise['done']) {
                            resolutionHandler(promise.value);
                        } else {
                            promiseValue = promise["value"];
                            if (!(promiseValue instanceof Promise)) {
                                promiseValue = new Promise((resolve) => resolve(promiseValue));
                            }

                            promiseValue.then(promiseCallback, promiseErrback);
                        }
                    }
                    appliableFunction = appliableFunction.apply(applyFunctionThisReference, []);
                    _0x573821(appliableFunction.next());
                }
            );
        };
    }

    var generator;
    if (this && this['__generator']) {
        generator = this['__generator'];
    } else {
        generator = function(callbackScopeObject, callbackFunction) {

            var isGeneratorExecuting, _0x4b4112, callerReturnValues;
            var callbackResponse = {
                'label': 0,
                'sent': function() {
                    if (1 & callerReturnValues[0]) {
                        throw callerReturnValues[1];
                    }
                    return callerReturnValues[1];
                },
                'trys': [],
                'ops': []
            };

            function responseHandlerGenerator(functionCallIndicator) {
                return function(currentResponseCode) {
                    function someFunc(callbackResultsArray) {
                        if (isGeneratorExecuting) {
                            throw new TypeError('Generator is already executing.');
                        }
                        while(callbackResponse) {
                            try {
                                isGeneratorExecuting = 1;

                                if (_0x4b4112) { //Minimum of first two calls, this is 0 which is false.
                                    if (2 & callbackResultsArray[0]) { //is _0x32303c[0] a 2,3,6 or 7?
                                        callerReturnValues = _0x4b4112.return;
                                    } else if (callbackResultsArray[0]) { //is it not a zero?
                                        if (_0x4b4112.throw) {
                                            callerReturnValues = _0x4b4112.throw;
                                        } else {
                                            callerReturnValues = _0x4b4112.return
                                            if (callerReturnValues) {
                                                callerReturnValues.call(_0x4b4112);
                                            }
                                            callerReturnValues = 0;
                                        }
                                    } else { //It's a zero, the initiator call was .next
                                        callerReturnValues = _0x4b4112.next;
                                    }
                                    
                                    if (callerReturnValues) { //Did we have an error?
                                        callerReturnValues = callerReturnValues.call(_0x4b4112, callbackResultsArray[1]);
                                        if (!callerReturnValues['done']) {
                                            return callerReturnValues;
                                        }
                                    }
                                }

                                _0x4b4112 = 0;
                                if (callerReturnValues) { //Null/undefined on first call
                                    callbackResultsArray = [2 & callbackResultsArray[0], callerReturnValues.value];
                                }

                                switch (callbackResultsArray[0]) {
                                    case 0:
                                    case 1:
                                        callerReturnValues = callbackResultsArray;
                                        break;
                                    case 4:
                                        callbackResponse.label++;
                                        return {
                                            'value': callbackResultsArray[1],
                                            'done': false,
                                        };
                                    case 5:
                                        callbackResponse.label++;
                                        _0x4b4112 = callbackResultsArray[1];
                                        callbackResultsArray = [0];
                                        continue; //Is a loop function, not a switch function. Go back to the top of he loop
                                    case 7:
                                        callbackResponse.ops.pop();
                                        callbackResultsArray = callbackResponse.trys.pop();
                                        continue;
                                    default:
                                        callerReturnValues = callbackResponse.trys
                                        if (!((callerReturnValues = callerReturnValues.length > 0 && callerReturnValues[callerReturnValues.length - 1]) || (6 !== callbackResultsArray[0] && 2 !== callbackResultsArray[0]))) {
                                            callbackResponse = 0;
                                            continue;
                                        }
                                        if (3 === callbackResultsArray[0] && (!callerReturnValues || (callbackResultsArray[1] > callerReturnValues[0] && callbackResultsArray[1] < callerReturnValues[3]))) {
                                            callbackResponse.label = callbackResultsArray[1];
                                            break;
                                        }
                                        if (6 === callbackResultsArray[0] && callbackResponse.label < callerReturnValues[1]) {
                                            callbackResponse.label = callerReturnValues[1];
                                            callerReturnValues = callbackResultsArray;
                                            break;
                                        }
                                        if (callerReturnValues && callbackResponse.label < callerReturnValues[2]) {
                                            callbackResponse.label = callerReturnValues[2];
                                            callbackResponse.ops.push(callbackResultsArray);
                                            break;
                                        }
                                        if (callerReturnValues[2]) {
                                            callbackResponse.ops.pop();
                                        }
                                        callbackResponse.trys.pop();
                                        continue;
                                }
                                callbackResultsArray = callbackFunction.call(callbackScopeObject, callbackResponse);
                            } catch (exception) {
                                callbackResultsArray = [6, exception];
                                _0x4b4112 = 0;
                            } finally {
                                callerReturnValues = 0
                                isGeneratorExecuting = 0;
                            }
                        }
                        if (5 & callbackResultsArray[0]) {
                            throw callbackResultsArray[1];
                        }
                        return {
                            'value': callbackResultsArray[0] ? callbackResultsArray[1] : undefined,
                            'done': true
                        };
                    };
                    
                    return someFunc([functionCallIndicator, currentResponseCode]);
                };
            }

            var responseObject = {
                'next': responseHandlerGenerator(0),
                'throw': responseHandlerGenerator(1),
                'return': responseHandlerGenerator(2)
            }

            if ("function" == typeof Symbol) {
                responseObject[Symbol.iterator] = () => this;
            }

            return responseObject;
        };
    }

    currentTokenManager["__esModule"] = true;
    getOrCreateExportsReferenceFunc(2).polyfill();
    
    var _0x1c174e = getOrCreateExportsReferenceFunc(5);
    getOrCreateExportsReferenceFunc(7);
    var _0x53302d = getOrCreateExportsReferenceFunc(8),
        consoleProxy = getOrCreateExportsReferenceFunc(9),
        _0x3c5e99 = getOrCreateExportsReferenceFunc(10),
        _0x402d51 = getOrCreateExportsReferenceFunc(11),
        currentCookieManager = getOrCreateExportsReferenceFunc(0);

    function getChallengeScriptSourceURL() {
        var challengeScriptSource = currentCookieManager["findChallengeScript"]();
        return currentCookieManager["stripQuery"](challengeScriptSource["src"]);
    }

    currentTokenManager["COOKIE_NAME"] = "reese84";
    currentTokenManager["COOKIE_NAME_SECONDARY"] = "x-d-token";

    var currentIncapsulaToken = function() {
        function incapsulaToken(tokenValue, renewalTime, renewsInSeconds, cookieDomain) {
            this["token"] = tokenValue;
            this['renewTime'] = renewalTime;
            this["renewInSec"] = renewsInSeconds;
            this["cookieDomain"] = cookieDomain;
        }

        incapsulaToken["fromTokenResponse"] = function(cookieObject) {
            var currentDate = new Date();
            currentDate["setSeconds"](currentDate["getSeconds"]() + cookieObject["renewInSec"]);
            
            return new incapsulaToken(cookieObject["token"], currentDate["getTime"](), cookieObject["renewInSec"], cookieObject["cookieDomain"]);
        };
        
        return incapsulaToken;
    }();

    function fetchCookieFromLocalStorage() {
        try {
            var fetchedCookie = localStorage["getItem"](currentTokenManager["COOKIE_NAME"]);
            return fetchedCookie ? JSON['parse'](fetchedCookie) : null;
        } catch (exception) {
            return null;
        }
    }

    function fetchCookieFromStorage() {
        var extractedCookie = currentCookieManager["extractCookie"](document['cookie'], currentTokenManager['COOKIE_NAME']);
        if (extractedCookie == null) {
            extractedCookie = currentCookieManager["extractCookie"](document["cookie"], currentTokenManager["COOKIE_NAME_SECONDARY"])
        }
        var fetchedCookie = fetchCookieFromLocalStorage();

        if (!extractedCookie) {
            return fetchedCookie
        }
        
        if (fetchedCookie && fetchedCookie["token"] === extractedCookie) {
            return fetchedCookie;
        }
        
        return new currentIncapsulaToken(extractedCookie, 0, 0, null);
    }
    currentTokenManager["extractTokenLocalStorage"] = fetchCookieFromLocalStorage;
    currentTokenManager['extractTokenStorage'] = fetchCookieFromStorage;

    var getObjectPrototypeSetter = function(startingObject) {
        function setObjectPrototype(methodName) {
            var constructor = this["constructor"];
            var targetObject = startingObject["call"](this, methodName) || this;
            var constructorPrototype = constructor["prototype"];

            if (Object["setPrototypeOf"]) {
                Object["setPrototypeOf"](targetObject, constructorPrototype)
            } else {
                targetObject['__proto__'] = constructorPrototype
            }
            return targetObject;
        }

        objectCreator(setObjectPrototype, startingObject)

        return setObjectPrototype;
    }(Error);

    currentTokenManager["RecoverableError"] = getObjectPrototypeSetter;

    var automationPayload = function() {};
    currentTokenManager['AutomationPayload'] = automationPayload;

    if (!!!currentTokenManager['CaptchaProvider']) {
        currentTokenManager['CaptchaProvider'] = {};
    }
    currentTokenManager['CaptchaProvider']["recaptcha"] = 'recaptcha';

    var captchaPayloadNoop = function() {};
    currentTokenManager["CaptchaPayload"] = captchaPayloadNoop;

    var requestMethods;
    var _0x2ee895 = function() {
        function _0x23b210(postbackUrl, httpClient, encryptionkeySha2) {
            this['httpClient'] = httpClient['bind'](window);

            if (typeof postbackUrl === "string") {
                this["postbackUrl"] = postbackUrl;
            } else {
                this["postbackUrl"] = postbackUrl();
            }
            this["tokenEncryptionKeySha2"] = encryptionkeySha2;
        };

        _0x23b210["prototype"]["validate"] = function(_0x194a1e) {
            return awaiter(this, function() {
                var tempTokenResponse, tempTokenResponseParser;
                return generator(this, function(callbackResponseObj) {
                        switch (callbackResponseObj.label) {
                            case 0:
                                tempTokenResponse = tokenResponse;
                                tempTokenResponseParser = tempTokenResponse["fromJson"];
                                return [4, _0x316937(this["httpClient"], this["postbackUrl"], _0x194a1e, this["tokenEncryptionKeySha2"])];
                            case 1:
                                return [2, tempTokenResponseParser.apply(tempTokenResponse, [callbackResponseObj.sent()])];
                        }
                    }
                );
            });
        };
        
        _0x23b210["prototype"]["automationCheck"] = function(_0x575fef) {
            return awaiter(this, function() {
                var tokenResponseObject, lastReturnValue;
                return generator(this, function(callbackResponseObj) {
                    switch (callbackResponseObj.label) {
                        case 0:
                            tokenResponseObject = tokenResponse.fromJson;
                            lastReturnValue = [4, _0x316937(this["httpClient"], this["postbackUrl"], _0x575fef, this["tokenEncryptionKeySha2"])];
                            return lastReturnValue;
                        case 1:
                            return [2, lastReturnValue.apply(tokenResponseObject, [callbackResponseObj.sent()])];
                    }
                });
            });
        };
        
        _0x23b210["prototype"]['submitCaptcha'] = function(_0x56d934) {
            return awaiter(this, function() {
                var tokenResponseObject, lastReturnValue;
                return generator(this, function(callbackResponseObj) {
                    switch (callbackResponseObj.label) {
                        case 0:
                            tokenResponseObject = tokenResponse.fromJson;
                            lastReturnValue = [4, _0x316937(this['httpClient'], this["postbackUrl"], _0x56d934, this["tokenEncryptionKeySha2"])];
                            return lastReturnValue;
                        case 1:
                            return [2, lastReturnValue.apply(tokenResponseObject, [callbackResponseObj.sent()])];
                    }
                });
            });
        };
        
        _0x23b210["prototype"]["tokenExpiryCheck"] = function(_0x59420a) {
            return awaiter(this, function() {
                var tokenResponseObject, lastReturnValue;
                return generator(this, function(callbackResponseObj) {
                    switch (callbackResponseObj.label) {
                        case 0:
                            tokenResponseObject = tokenResponse.fromJson;
                            lastReturnValue = [4, _0x316937(this['httpClient'], this["postbackUrl"], _0x59420a, this["tokenEncryptionKeySha2"])]
                            return lastReturnValue;
                        case 1:
                            return [2, lastReturnValue.apply(tokenResponseObject, [callbackResponseObj.sent()])];
                    }
                });
            });
        };
        
        return _0x23b210;
    }();

    function _0x316937(httpClient, postbackUrl, requestBodyObject, encyptionKeySha2) {
        return awaiter(this, function() {
            var hostname, requestBody, headers, domain, urlWithQuery, response, errorResponse;
            return generator(this, function(callbackResponseObj) {
                switch (callbackResponseObj.label) {
                    case 0:
                        callbackResponseObj.trys.push([0, 2, undefined, 3]);
                        hostname = window["location"]["hostname"];
                        requestBody = JSON.stringify(requestBodyObject, jsonStringUndefinedReplace);

                        headers = {
                            'Accept': "application/json; charset=utf-8",
                            'Content-Type': 'text/plain; charset=utf-8'
                        };

                        if (encyptionKeySha2) {
                            headers["x-d-test"] = encyptionKeySha2;
                        }

                        domain = 'd=' + hostname;
                        urlWithQuery = currentCookieManager["appendQueryParam"](postbackUrl, domain)

                        return  [4, httpClient(urlWithQuery, {
                            'body': requestBody,
                            'headers': headers,
                            'method': requestMethods["Post"]
                        })];
                    case 1:
                        response = callbackResponseObj["sent"]();
                        if (response['ok']) {
                            return [2, response["json"]()];
                        }
                        throw new Error("Non-ok status code: " + response['status']);
                    case 2:
                        errorResponse = callbackResponseObj["sent"]();
                        throw new getObjectPrototypeSetter("Request error for 'POST " + postbackUrl + "': " + errorResponse);
                    case 3:
                        return [2];
                }
            });
        });
    };

    
    currentTokenManager["BonServer"] = _0x2ee895;

    if (!requestMethods) {
        requestMethods = {};
    }

    function setupRequestMethods(incoming) {
        incoming["Get"] = "GET";
        incoming["Post"] = "POST";
    }

    setupRequestMethods(requestMethods);

    var tokenResponse = function() {
        function tokenResponseConstructor(token, secondsToRenewal, cookieDomain, debugEnabled) {
            this['token'] = token;
            this["renewInSec"] = secondsToRenewal;
            this["cookieDomain"] = cookieDomain;
            this["debug"] = debugEnabled;
        }

        tokenResponseConstructor["fromJson"] = function(objectToConvert) {
            if ('string' != typeof objectToConvert["token"]) {
                if (null !== objectToConvert['token']) {
                    throw new Error('Unexpected token response format');
                } else if ("number" != typeof objectToConvert['renewInSec']) {
                    throw new Error('Unexpected token response format');
                } else if ( 'string' != typeof objectToConvert['cookieDomain'] && null !== objectToConvert["cookieDomain"]) {
                    throw new Error('Unexpected token response format');
                } else if ("string" != typeof objectToConvert['debug'] && undefined !== objectToConvert["debug"]) {
                    throw new Error('Unexpected token response format');
                }
            }
            return objectToConvert;
        };
        
        return tokenResponseConstructor;
    }();
    
    currentTokenManager["TokenResponse"] = tokenResponse;

    var setSolution = function(interrogation, version) {
        this["interrogation"] = interrogation;
        this["version"] = version;
    };
    currentTokenManager["Solution"] = setSolution;

    var prepSolutionResponse = function(solution, oldToken, error, performance) {
        if (oldToken === undefined) {
            oldToken = null;
        }

        if (error === undefined) {
            error = null;
        }

        if (performance ===  undefined) {
            performance = null;
        }
        
        this["solution"] = solution;
        this["old_token"] = oldToken;
        this["error"] = error;
        this["performance"] = performance;
    };

    currentTokenManager['SolutionResponse'] = prepSolutionResponse;
    currentTokenManager["PRIMARY_COOKIE"] = "none_secure";
    currentTokenManager['SECONDARY_COOKIE'] = "";


    var incapsulaProtectionObject = function() {
        function protectionObject(scheduler, _0x17756d) {
            if (scheduler === undefined) {
                scheduler = new _0x3c5e99['RobustScheduler']();
            }

            if (_0x17756d === undefined) {
                _0x17756d = new _0x2ee895(getChallengeScriptSourceURL, window["fetch"], null);
            }
            this["currentToken"] = null;
            this["currentTokenExpiry"] = new Date();
            this.currentTokenError = null;
            this["waitingOnToken"] = [];
            this["started"] = false;
            this["scheduler"] = scheduler;
            this["bon"] = _0x17756d;
            this["timer"] = _0x402d51['timerFactory']();
        }

        protectionObject["prototype"]["token"] = function(timeoutValue) {
            return awaiter(this, function() {
                var currentDate;
                var _0x422d14 = this;
                return generator(this, function(callbackRespnseObj) {
                    switch (callbackRespnseObj.label) {
                        case 0:
                            if (currentCookieManager["isSearchEngine"](window['navigator']['userAgent'])) {
                                return [2, ''];
                            }
                            if (!this["started"]) {
                                throw new Error("Protection has not started.");
                            }

                            currentDate = new Date();

                            if (this['currentToken'] != null && currentDate < this["currentTokenExpiry"]) {
                                return [2, this["currentToken"]];
                            } else if (this.currentTokenError != null) {
                                return [2, Promise.reject(this.currentTokenError)];
                            } else {
                                return [4, new Promise(
                                        function(successHandler, errorHandler) {
                                            _0x422d14["waitingOnToken"]['push']([successHandler, errorHandler]);
                                            if (timeoutValue !== undefined) {
                                                setTimeout(function() {
                                                    return errorHandler(new Error("Timeout while retrieving token"));
                                                }, timeoutValue);
                                            }
                                        }
                                    )
                                ];
                            }

                        case 1:
                            return [2, callbackRespnseObj['sent']()];
                    }
                });
            });
        };

        protectionObject["prototype"]["submitCaptcha"] = function(captchaProvider, captchaPayload, captchaTimeout, captchaData) {
            return awaiter(this, function() {
                var thisRef = this;
                return generator(this, function(callbackResponseObj) {
                    switch (callbackResponseObj.label) {
                        case 0:
                            return [4, new Promise(
                                function(_0x5c6381, _0x49f8b4) {
                                    return awaiter(thisRef, function() {
                                        var captchaToken, _0x54190c, _0x24258e;
                                        return generator(this, function(callbackResponseObj) {
                                            switch (callbackResponseObj.label) {
                                                case 0:
                                                    callbackResponseObj["trys"]["push"]([0, 3, , 4]);
                                                    setTimeout(function() {
                                                        _0x49f8b4(new Error("submitCaptcha timed out"));
                                                    }, captchaTimeout);
                                                    if (!this['started']) {
                                                        this['start']();
                                                    }
                                                    return [4, this["token"](captchaTimeout)];
                                                case 1:
                                                    captchaToken = callbackResponseObj["sent"]();
                                                    return [4, this["bon"]["submitCaptcha"]({
                                                        'data': captchaData,
                                                        'payload': captchaPayload,
                                                        'provider': captchaProvider,
                                                        'token': captchaToken
                                                    })];
                                                case 2:
                                                    _0x54190c = callbackResponseObj['sent']();
                                                    this["setToken"](_0x54190c);
                                                    _0x5c6381(_0x54190c['token']);
                                                    return [3, 4];
                                                case 3:
                                                    _0x24258e = callbackResponseObj["sent"]();
                                                    _0x49f8b4(_0x24258e);
                                                    return [3, 4];
                                                case 4:
                                                    return [2];
                                            }
                                        }
                                    );
                                });
                            })];
                        case 1:
                            return [2, callbackResponseObj['sent']()];
                    }
                });
            });
        };

        protectionObject['prototype']["stop"] = function() {
            this['scheduler']["stop"]();
        };

        protectionObject['prototype']['start'] = function(_0x2efab6) {
            var thisRef = this;

            if (undefined === _0x2efab6) {
                _0x2efab6 = false;
            }

            if (currentCookieManager["isSearchEngine"](window["navigator"]["userAgent"])) {
                return;
            }

            this["started"] = true;
            if ("loading" === document["readyState"]) {
                document["addEventListener"]("DOMContentLoaded", function() {
                    return thisRef["startInternal"](_0x2efab6);
                });
            } else {
                this["startInternal"](_0x2efab6);
            }
        };
        
        protectionObject["prototype"]["startInternal"] = function(_0x3090dd) {
            return awaiter(this, function() {
                var fetchedCookie, _0x4781c8, _0x8bad6c, _0x10d9cf, _0x32e4fa, tokenWaiters, waiter;
                return generator(this, function(callbackResponseObj) {
                    switch (callbackResponseObj.label) {
                        case 0:
                            this.timer.start("total");
                            fetchedCookie = fetchCookieFromStorage();
                            callbackResponseObj.label = 1;
                        case 1:
                            callbackResponseObj["trys"]["push"]([1, 5, 6]);
                            if (_0x3090dd || !fetchedCookie) {
                                return [3, 3];
                            } else {
                                _0x4781c8 = new Date(fetchedCookie['renewTime']);
                                _0x8bad6c = new Date();
                                if (_0x8bad6c <= _0x4781c8 && (_0x4781c8['getTime']() - _0x8bad6c["getTime"]()) / 1000 <= fetchedCookie["renewInSec"]) {
                                    return [4, this["bon"]["tokenExpiryCheck"](fetchedCookie["token"])];
                                } else {
                                    return [3, 3];
                                }
                            }
                        case 2:
                            _0x10d9cf = callbackResponseObj['sent']();
                            this["setToken"](_0x10d9cf);
                            this["runAutomationCheck"]();
                            this["timer"]["stop"]("total");
                            return [2];
                        case 3:
                            return [4, this['updateToken']()];
                        case 4:
                            callbackResponseObj["sent"]();
                            this["runAutomationCheck"]();
                            return [3, 6];
                        case 5:
                            _0x32e4fa = callbackResponseObj["sent"]();
                            consoleProxy["log"]('error: ' + _0x32e4fa + " [ " + _0x32e4fa["message"] + ' ]');
                            this["currentToken"] = null;
                            this.currentTokenError = _0x32e4fa;
                            tokenWaiters = this['waitingOnToken'];

                            for (let i = 0; i < tokenWaiters["length"]; i++) {
                                waiter = tokenWaiters[i];
                                waiter[1](_0x32e4fa);
                            }
                            return [3, 6];
                        case 6:
                            this["timer"]['stop']("total");
                            return [2];
                    }
                });
            });
        }

        protectionObject["prototype"]["runAutomationCheck"] = function() {
            var _0x20fe7 = this;
            this["timer"]["start"]('ac'), _0x53302d["automationCheck"](function(_0x1f3d6b) {
                return awaiter(_0x20fe7, function() {
                    var cookie, _0x3b9720, _0x3c8dff;
                    return generator(this, function(callbackResponseObj) {
                        switch (callbackResponseObj.label) {
                            case 0:
                                callbackResponseObj.trys.push([0, 2, , 3])
                                cookie = fetchCookieFromStorage()
                                return [4, this["bon"]["automationCheck"](
                                        {
                                            'a': _0x1f3d6b,
                                            't': cookie ? cookie["token"] : null
                                        }
                                    )
                                ];
                            case 1:
                                _0x3b9720 = callbackResponseObj.sent();
                                this["setToken"](_0x3b9720);
                                return [3, 3];
                            case 2:
                                _0x3c8dff = callbackResponseObj.sent();
                                consoleProxy["log"](_0x3c8dff);
                                return [3, 3];
                            case 3:
                                return [2];
                        }
                    });
                });
            }), this.timer.stop('ac');
        };

        protectionObject["prototype"]["setToken"] = function(tokenResponse) {
            var _0x3c5fef = this,
                cookieSecurityType = function() {
                    switch (currentTokenManager["PRIMARY_COOKIE"]) {
                        case "legacy":
                        case 'lax':
                        case "none_secure":
                            return currentTokenManager["PRIMARY_COOKIE"];
                        default:
                            return 'lax';
                    }
                }(),
                secondaryCookieSecurityType = function() {
                    switch (currentTokenManager["SECONDARY_COOKIE"]) {
                        case "legacy":
                        case "lax":
                        case "none_secure":
                            return currentTokenManager['SECONDARY_COOKIE'];
                        default:
                            return null;
                    }
                }();
            if (null !== tokenResponse["token"]) {
                currentCookieManager["deleteCookie"](currentTokenManager["COOKIE_NAME"]);
                currentCookieManager["deleteCookie"](currentTokenManager["COOKIE_NAME_SECONDARY"]);
                currentCookieManager["setCookie"](currentTokenManager['COOKIE_NAME'], tokenResponse["token"], 2592000, tokenResponse["cookieDomain"], cookieSecurityType);
                if (null != secondaryCookieSecurityType) {
                    currentCookieManager["setCookie"](currentTokenManager["COOKIE_NAME_SECONDARY"], tokenResponse['token'], 2592000, tokenResponse["cookieDomain"], secondaryCookieSecurityType);
                }

                try {
                    localStorage["setItem"](currentTokenManager["COOKIE_NAME"], JSON['stringify'](currentIncapsulaToken["fromTokenResponse"](tokenResponse)));
                } catch (exception) {}
            }
            this["currentToken"] = tokenResponse["token"], this.currentTokenError = null;
            var _0x5bfadc = new Date();
            _0x5bfadc['setSeconds'](_0x5bfadc['getSeconds']() + tokenResponse['renewInSec']), this["currentTokenExpiry"] = _0x5bfadc;
            var _0x592671 = Math["max"](0, tokenResponse.renewInSec - 10);
            if (_0x592671 > 0) {
                for (let i = 0, waiter = this.waitingOnToken; i < waiter['length']; i++) {
                    waiter[i][0](tokenResponse['token']);
                }
            }
            this['scheduler']["runLater"](function() {
                return _0x3c5fef["updateToken"]();
            }, 1000 * _0x592671);
        };

        protectionObject["prototype"]["solve"] = function() {
            return awaiter(this, function() {
                var _0x19a239, interrogationType;
                return generator(this, function(callbackResponseObj) {
                    switch (callbackResponseObj.label) {
                        case 0:
                            _0x19a239 = _0x1c174e['interrogatorFactory'](this.timer);
                            return [4, new Promise(_0x19a239["interrogate"])];
                        case 1:
                            interrogationType = callbackResponseObj["sent"]();
                            return [2, new setSolution(interrogationType, "stable")];
                    }
                });
            });
        };

        protectionObject["prototype"]['getToken'] = function() {
            return awaiter(this, function() {
                var cookieValue, _0x243d07, solution, _0x52a2e4, _0x4f5ea5;
                return generator(this, function(callbackResponseObj) {
                    switch (callbackResponseObj.label) {
                        case 0:
                            cookieValue = fetchCookieFromStorage();
                            callbackResponseObj.label = 1;
                        case 1:
                            callbackResponseObj.trys.push([1, 3, , 4]);
                            return [4, this["solve"]()];
                        case 2:
                            solution = callbackResponseObj.sent();
                            let prepSolutionToken = cookieValue ? cookieValue.token : null;
                            _0x243d07 = new prepSolutionResponse(solution, prepSolutionToken, null, this.timer.summary());
                            return [3, 4];
                        case 3:
                            _0x52a2e4 = callbackResponseObj.sent();
                            let prepSolutionToken = cookieValue ? cookieValue.token : null;
                            _0x243d07 = new prepSolutionResponse(null, prepSolutionToken, "stable error: " + _0x52a2e4["toString"]() + '\x0a' + _0x52a2e4["stack"], null);
                            return [3, 4];
                        case 4:
                            return [4, this["bon"]['validate'](_0x243d07)];
                        case 5:
                            _0x4f5ea5 = callbackResponseObj.sent()
                            if (_0x4f5ea5['debug']) {
                                console["log"]('tokenResponse.debug: ' + _0x4f5ea5['debug']);
                            }
                            return [2, _0x4f5ea5];
                    }
                });
            });
        };

        protectionObject['prototype']["updateToken"] = function() {
            return awaiter(this, function() {
                var _0x180ff0, _0x2afc66 = this;
                return generator(this, function(callbackResponseObj) {
                    switch (callbackResponseObj.label) {
                        case 0:
                            return [4, _0x3c5e99["retry"](this["scheduler"], function() {
                                return _0x2afc66["getToken"]();
                            }, function(_0x2fa74c) {
                                return _0x2fa74c instanceof getObjectPrototypeSetter;
                            })];
                        case 1:
                            _0x180ff0 = callbackResponseObj["sent"]();
                            this["setToken"](_0x180ff0);
                            return [2];
                    }
                });
            });
        };

        return protectionObject;
    }();
    currentTokenManager["Protection"] = incapsulaProtectionObject;
};

reeseCallParamArray[2] = function(_0x2fda36, _0x371af5, getOrCreateExportsReferenceFunc) {
    let someFunc = function(thirdExportsReference, global) {
        var _0x359dc7 = function() {
            'use strict';

            function isFunction(testFunction) {
                return "function" == typeof testFunction;
            }

            var isArray = Array.isArray || ((arrayLikeObj) => "[object Array]" === Object.prototype.toString.call(arrayLikeObj));
                
            var maxUsedArraySpace = 0;
            var _0x5f163a = undefined;
            var scheduler = undefined;
            var _0x6539dd = new Array(1000);

            var asapRunner = function(someFunction, functionParam) {
                _0x6539dd[maxUsedArraySpace] = someFunction;
                _0x6539dd[maxUsedArraySpace + 1] = functionParam;
                maxUsedArraySpace += 2;
                if (2 === maxUsedArraySpace) { //Is this the first time the function was called?
                    if (scheduler) {
                        scheduler(clearArrayAndRunFunctions);
                    } else {
                        0x4cf28d();
                    }
                }
            };
            var windowReference = 'undefined' != typeof window ? window : undefined;
            var safeWindowReference = windowReference || {};
            var mutationObserver = safeWindowReference["MutationObserver"] || safeWindowReference['WebKitMutationObserver'];
            var isThirdExportProcessObject = typeof self === 'undefined' && thirdExportsReference !== undefined  && "[object process]" === {}.toString.call(thirdExportsReference);
            var isMessageChannelDefined = typeof Uint8ClampedArray != 'undefined' && typeof importScripts != "undefined" && typeof MessageChannel != "undefined";

            function runFunctionWithTimeout() {
                var setTimeoutRef = setTimeout;
                return function() {
                    return setTimeoutRef(clearArrayAndRunFunctions, 1);
                };
            }

            function clearArrayAndRunFunctions() {
                for (let i = 0; i < maxUsedArraySpace; i += 2) {
                    _0x6539dd[i](_0x6539dd[i + 1]);
                    _0x6539dd[i] = undefined;
                    _0x6539dd[i + 1] = undefined;
                }
                maxUsedArraySpace = 0;
            }
            var messageChannelInstance, _0x3c57af, mutationObserverInstance, textNode, clearArrayWrapper = undefined;

            function thenFunction(_0x49cdf9, _0x57465d) {
                var thisReference = this;
                var newThis = new this[("constructor")](noop);
                if (newThis[promiseID] === undefined) {
                    resetPromise(newThis);
                }
                var currentState = thisReference['_state'];
                if (currentState) {
                    var currentArgument = arguments[currentState - 1];
                    asapRunner(function() {
                        return _0x3f83a1(currentState, newThis, currentArgument, thisReference["_result"]);
                    });
                } else {
                    _0x505c93(thisReference, newThis, _0x49cdf9, _0x57465d);
                }
                return newThis;
            }

            function isThis(potentialThis) {
                if (potentialThis && "object" == typeof potentialThis && potentialThis["constructor"] === this) {
                    return potentialThis;
                }

                var _0x1e3cf4 = new this(noop);
                _resolve(_0x1e3cf4, potentialThis);
                return _0x1e3cf4;
            }

            if (isThirdExportProcessObject) {
                clearArrayWrapper = function() {
                    return thirdExportsReference["nextTick"](clearArrayAndRunFunctions);
                };
            } else if (mutationObserver) {
                _0x3c57af = 0;
                mutationObserverInstance = new mutationObserver(clearArrayAndRunFunctions);
                textNode = document["createTextNode"]('');
                mutationObserverInstance.observe(textNode, {
                    'characterData': true
                });
                
                clearArrayWrapper = function() {
                    _0x3c57af++;
                    _0x3c57af = _0x3c57af % 2;
                    textNode['data'] = _0x3c57af;
                };
            } else if (isMessageChannelDefined) {
                messageChannelInstance = new MessageChannel();
                
                messageChannelInstance["port1"]['onmessage'] = clearArrayAndRunFunctions;
                
                clearArrayWrapper = function() {
                    return messageChannelInstance["port2"]["postMessage"](0);
                }
            } else if (undefined === windowReference) {
                clearArrayWrapper = function() {
                    try {
                        var requireVertex = Function("return this")()["require"]('vertx');
                        _0x5f163a = requireVertex["runOnLoop"] || requireVertex["runOnContext"];
                        if (_0x5f163a !== undefined) {
                            return function() {
                                _0x5f163a(clearArrayAndRunFunctions);
                            };
                        } else {
                            return runFunctionWithTimeout();
                        }
                    } catch (exception) {
                        return runFunctionWithTimeout();
                    }
                }
                
                clearArrayWrapper = clearArrayWrapper();
            } else {
                clearArrayWrapper = runFunctionWithTimeout();
            }

            
            var promiseID = Math.random().toString(24).substring(2);

            function noop() {}

            function _0x3098f8(_0x544328, _0x5e1e8c, _0x25d88b) {
                if (_0x5e1e8c.constructor === _0x544328.constructor && _0x25d88b === thenFunction && _0x5e1e8c.constructor.resolve === isThis) {
                    let temp = function(_0x54cfd6, _0x1600b1) {
                        if (_0x1600b1._state === 1) {
                            fulfill(_0x54cfd6, _0x1600b1["_result"]);
                        } else if (_0x1600b1["_state"] === 2) {
                            _reject(_0x54cfd6, _0x1600b1['_result']);
                        } else {
                            _0x505c93(_0x1600b1, undefined, function(_0x11b216) {
                                return _resolve(_0x54cfd6, _0x11b216);
                            }, function(_0x29c7ca) {
                                return _reject(_0x54cfd6, _0x29c7ca);
                            });
                        }
                    };
                    temp(_0x544328, _0x5e1e8c);
                } else if (_0x25d88b === undefined) {
                    fulfill(_0x544328, _0x5e1e8c)
                } else if (isFunction(_0x25d88b)) {
                    let isFunctionInner = function(_0x37d8e4, _0x2fa5cb, _0xa125d6) {
                        let tempOuterFunction = function(_0xc16737) {
                            var flag = false;
                            var safeFunctionRunner = function(targetFunction, functionThisArg, functionParam1, functionParam2) {
                                try {
                                    targetFunction["call"](functionThisArg, functionParam1, functionParam2);
                                } catch (exception) {
                                    return exception;
                                }
                            };

                            let tempFunction1 = function(_0x17c723) {
                                if (flag) {
                                    return;
                                }
                                flag = true;
                                if (_0x2fa5cb !== _0x17c723) {
                                    _resolve(_0xc16737, _0x17c723);
                                } else {
                                    fulfill(_0xc16737, _0x17c723);
                                }
                            };

                            let tempFunction2 = function(_0xeb5d5c) {
                                if (flag) {
                                    return;
                                }

                                flag = true;
                                _reject(_0xc16737, _0xeb5d5c);
                            };

                            safeFunctionRunner(_0xa125d6, _0x2fa5cb, tempFunction1, tempFunction2, _0xc16737['_label']);

                            if (!flag && safeFunctionRunner) {
                                flag = true;
                                _reject(_0xc16737, safeFunctionRunner);
                            }
                            
                        };
                        asapRunner(tempOuterFunction, _0x37d8e4);
                    };
                    isFunctionInner(_0x544328, _0x5e1e8c, _0x25d88b);
                } else {
                    0x1887c1(_0x544328, _0x5e1e8c);
                }
            }

            function _resolve(_0x185497, _0xea447) {

                if (_0x185497 === _0xea447) {
                    _reject(_0x185497, new TypeError("You cannot resolve a promise with itself"));
                    return;
                }

                let _0x279563 = _0xea447;
                let _0x6a93d8 = typeof _0x279563;

                if (null === _0x279563 || ("object" !== _0x6a93d8 && "function" !== _0x6a93d8)) {
                    fulfill(_0x185497, _0xea447);
                } else {
                    var _0x36f7af = undefined;
                    try {
                        _0x36f7af = _0xea447["then"];
                    } catch (exception) {
                        _reject(_0x185497, exception);
                        return undefined;
                    }
                    _0x3098f8(_0x185497, _0xea447, _0x36f7af);
                }
            }

            function _0x49c371(_0x9c1e64) {
                if (_0x9c1e64._onerror) {
                    _0x9c1e64._onerror(_0x9c1e64._result)
                }
                _0x35d9a4(_0x9c1e64);
            }

            function fulfill(_0x92008b, result) {
                if (_0x92008b._state !== undefined) {
                    return;
                }

                _0x92008b._result = result;
                _0x92008b._state = 1;
                if (_0x92008b._subscribers.length !== 0) {
                    asapRunner(_0x35d9a4, _0x92008b);
                }
            }

            function _reject(_0x81011a, _0x3ba035) {
                if ( _0x81011a._state !== undefined) {
                    return;
                }
                _0x81011a._state = 2;
                _0x81011a._result = _0x3ba035;
                asapRunner(_0x49c371, _0x81011a);
            }

            function _0x505c93(_0xa07ead, _0x28b57d, _0x4b7fd0, _0x4eb62e) {
                let _0xc270b1 = _0xa07ead["_subscribers"];
                let _0xc503f = _0xc270b1["length"];
                _0xa07ead["_onerror"] = null;
                _0xc270b1[_0xc503f] = _0x28b57d;
                _0xc270b1[_0xc503f + 1] = _0x4b7fd0;
                _0xc270b1[_0xc503f + 2] = _0x4eb62e;

                if (_0xc503f === 0 && _0xa07ead._state) {
                    asapRunner(_0x35d9a4, _0xa07ead);
                }
            }

            function _0x35d9a4(_0x25f4b0) {
                let _0x371378 = _0x25f4b0["_subscribers"];
                let _0x5f46df = _0x25f4b0["_state"];
                if (0 === _0x371378.length) {
                    return;
                }

                let _0x427c98 = undefined;
                let _0x25f3dd = undefined;
                let _0x5a30b0 = _0x25f4b0["_result"];

                for (var _0x55005b = 0; _0x55005b < _0x371378.length; _0x55005b += 3) {
                    _0x427c98 = _0x371378[_0x55005b];
                    _0x25f3dd = _0x371378[_0x55005b + _0x5f46df];
                    if (_0x427c98) {
                        _0x3f83a1(_0x5f46df, _0x427c98, _0x25f3dd, _0x5a30b0);
                    } else {
                        _0x25f3dd(_0x5a30b0);
                    }
                }
                _0x25f4b0["_subscribers"]['length'] = 0;
            }

            function _0x3f83a1(_0x2a894d, _0x29a74e, _0x3b74e7, _0x3fda0f) {
                var _0x47dd42 = isFunction(_0x3b74e7);
                let _0x224e15 = undefined;
                let _0x439ef5 = undefined;
                let _0x4dfb1d = true;

                if (_0x47dd42) {
                    try {
                        _0x224e15 = _0x3b74e7(_0x3fda0f);
                    } catch (exception) {
                        _0x4dfb1d = false;
                        _0x439ef5 = exception;
                    }
                    if (_0x29a74e === _0x224e15) {
                        _reject(_0x29a74e, new TypeError("A promises callback cannot return that same promise."))
                        return undefined;
                    }
                } else {
                    _0x224e15 = _0x3fda0f;
                }

                if (_0x29a74e._state !== undefined) {
                    return;
                }

                if (_0x47dd42 && _0x4dfb1d) {
                    _resolve(_0x29a74e, _0x224e15)
                } else if (_0x4dfb1d === false) {
                    _reject(_0x29a74e, _0x439ef5);
                } else if  (_0x2a894d === 1) {
                    fulfill(_0x29a74e, _0x224e15);
                } else if (_0x2a894d === 2) {
                    _reject(_0x29a74e, _0x224e15);
                }
            }

            var currentPromiseID = 0;
            function resetPromise(somePromise) {
                somePromise[promiseID] = currentPromiseID++;
                somePromise._state = undefined;
                somePromise._result = undefined;
                somePromise._subscribers = [];
            }


            var PromiseEnumerator = function() {
                    function PromiseEnumerator(instanceConstructor, someArray) {
                        this._instanceConstructor = instanceConstructor;
                        this.promise = new instanceConstructor(noop);
                        if (!!this.promise[promiseID]) {
                            resetPromise(this.promise);
                        }

                        if (isArray(someArray)) {
                            this.length = someArray.length;
                            this._remaining = someArray.length;
                            this._result = new Array(this.length);

                            if (0 === this.length) {
                                fulfill(this.promise, this["_result"]);
                            } else {
                                this.length = this.length || 0;
                                this["_enumerate"](someArray)
                                if (0 === this['_remaining']) {
                                    fulfill(this["promise"], this['_result']);
                                }
                            }
                        } else {
                            _reject(this["promise"], new Error("Array Methods must be provided an Array"));
                        }
                    }
                    PromiseEnumerator["prototype"]["_enumerate"] = function(inputPromises) {
                        for (let i = 0; this['_state'] === undefined && i < inputPromises['length']; i++) {
                            this["_eachEntry"](inputPromises[i], i)
                        }
                    };

                    PromiseEnumerator["prototype"]["_eachEntry"] = function(entry, currentIndex) {
                        var instanceConstructor = this['_instanceConstructor'], resolvedConstructor = instanceConstructor["resolve"];
                        if (resolvedConstructor === isThis) {
                            var _0x1976e1 = undefined;
                            let _0x4ba367 = undefined;
                            let wasException = false;
                            try {
                                _0x1976e1 = entry['then'];
                            } catch (exception) {
                                wasException = true;
                                _0x4ba367 = exception;
                            }
                            if (_0x1976e1 === thenFunction && undefined !== entry["_state"]) {
                                this["_settledAt"](entry["_state"], currentIndex, entry["_result"]);
                            } else if ('function' != typeof _0x1976e1) {
                                this["_remaining"]--;
                                this["_result"][currentIndex] = entry;
                            } else if (instanceConstructor === es6Promise) {
                                var _0x4c0bbe = new instanceConstructor(noop);
                                if (wasException) {
                                    _reject(_0x4c0bbe, _0x4ba367);
                                } else {
                                    _0x3098f8(_0x4c0bbe, entry, _0x1976e1);
                                }
                                
                                this._willSettleAt(_0x4c0bbe, currentIndex);
                            } else {
                                let innerSomeFuncSettle = function(_0x5650bd) {
                                    return _0x5650bd(entry);
                                };
                                this._willSettleAt(new instanceConstructor(innerSomeFuncSettle), currentIndex);
                            }
                        } else {
                            this._willSettleAt(resolvedConstructor(entry), currentIndex);
                        }
                    };
                    
                    PromiseEnumerator["prototype"]["_settledAt"] = function(_0xc5eb0f, _0x114cef, _0x1c53f9) {
                        var _0x545476 = this["promise"];
                        if (undefined === _0x545476["_state"]) {
                            this["_remaining"]--;
                            if (2 === _0xc5eb0f) {
                                _reject(_0x545476, _0x1c53f9);
                            } else {
                                this['_result'][_0x114cef] = _0x1c53f9;
                            }
                        }

                        if (0 === this['_remaining']) {
                            fulfill(_0x545476, this['_result']);
                        }
                    };
                    
                    PromiseEnumerator['prototype']["_willSettleAt"] = function(_0x39e774, _0x362745) {
                        var _0x246949 = this;
                        _0x505c93(
                            _0x39e774, undefined,
                            function(_0x18ca4d) {
                                return _0x246949['_settledAt'](1, _0x362745, _0x18ca4d);
                            },
                            function(_0x4a24af) {
                                return _0x246949['_settledAt'](2, _0x362745, _0x4a24af);
                            }
                        );
                    };
                    
                    return PromiseEnumerator;
            };
            PromiseEnumerator = PromiseEnumerator();


            var es6Promise = function() {
                function es6PromiseConstructor(promiseResolver) {
                    this[promiseID] = currentPromiseID++;
                    this['_state'] = undefined;
                    this['_result'] = undefined;
                    this["_subscribers"] = [];
                    if (noop === promiseResolver) {
                        return;
                    }

                    if ("function" != typeof promiseResolver) {
                        ! function() {
                            throw new TypeError('You must pass a resolver function as the first argument to the promise constructor');
                        }();
                    }

                    if (this instanceof es6PromiseConstructor) {
                        let initializePromise = function(currentPromise, promiseResolver) {
                            try {
                                promiseResolver(function resolvePromise(promiseValue) {
                                    _resolve(currentPromise, promiseValue);
                                }, function rejectPromise(rejectionReason) {
                                    _reject(currentPromise, rejectionReason);
                                });
                            } catch (exception) {
                                _reject(currentPromise, exception);
                            }
                        };

                        initializePromise(this, promiseResolver);
                    } else {
                        ! function() {
                            throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
                        }();
                    }
                }
                es6PromiseConstructor["prototype"]["catch"] = function(rejectionCallback) {
                    return this.then(null, rejectionCallback);
                };
                
                es6PromiseConstructor["prototype"]["finally"] = function(resolutionValue) {
                    var somePromise = this["constructor"];

                    if (isFunction(resolutionValue)) {
                        return this["then"](
                            function(successValue) {
                                return somePromise.resolve(resolutionValue()).then(function() {
                                    return successValue;
                                });
                            },
                            function(rejectionReason) {
                                return somePromise.resolve(resolutionValue()).then(function() {
                                    throw rejectionReason;
                                });
                            }
                        );
                    } else {
                        return this['then'](resolutionValue, resolutionValue);
                    }
                };
                return es6PromiseConstructor;
            }();

            es6Promise["prototype"]["then"] = thenFunction;
            es6Promise["all"] = function(arrayPromises) {
                return new PromiseEnumerator(this, arrayPromises)['promise'];
            };
            es6Promise["race"] = function(promisesToRace) {
                var PromiseConstructor = this;
                if (isArray(promisesToRace)) {
                    return new PromiseConstructor(function(promiseResolution, promiseRejection) {
                        let arrayLength = promisesToRace['length']
                        for (let i = 0; i < arrayLength; i++) {
                            PromiseConstructor.resolve(promisesToRace[i]).then(promiseResolution, promiseRejection);
                        }
                    });
                } else {
                    return new PromiseConstructor(function(promiseResolution, promiseRejection) {
                        return promiseRejection(new TypeError("You must pass an array to race."));
                    });
                }
            };
            
            es6Promise["resolve"] = isThis;
            es6Promise["reject"] = function(rejectionReason) {
                var somePromise = new this(noop);
                _reject(somePromise, rejectionReason);
                return somePromise;
            };
            es6Promise["_setScheduler"] = function(newScheduler) {
                scheduler = newScheduler;
            };
            
            es6Promise['_setAsap'] = function(newAsapRunner) {
                asapRunner = newAsapRunner;
            };
            
            es6Promise["_asap"] = asapRunner;
            es6Promise['polyfill'] = function() {
                var local = undefined;
                if (undefined !== global) {
                    local = global;
                } else if ("undefined" != typeof self) {
                    local = self;
                } else {
                    try {
                        local = Function("return this")();
                    } catch (exception) {
                        throw new Error('polyfill failed because global object is unavailable in this environment');
                    }
                }

                var localPromise = local.Promise;
                if (localPromise) {
                    var promiseToString = null;
                    try {
                        promiseToString = Object.prototype.toString.call(localPromise.resolve());
                    } catch (exception) {}

                    if ("[object Promise]" === promiseToString && !localPromise.cast) {
                        return;
                    }
                }
                local.Promise = es6Promise;
            };
            es6Promise.Promise = es6Promise;
            return es6Promise;
        };
        _0x2fda36["exports"] = _0x359dc7();
    };
    let thirdExports = getOrCreateExportsReferenceFunc(3);
    let fourthExports = getOrCreateExportsReferenceFunc(4);
    
    someFunc.call(this, thirdExports, fourthExports);
};

reeseCallParamArray[3] = function(exportsContainer_3, desiredProcessVariable, getOrCreateExportsReferenceFunc) {
    var setTimeoutProxy, clearTimeoutProxy;
    desiredProcessVariable = {};

    function setTimeoutError() {
        throw new Error("setTimeout has not been defined");
    }

    function clearTimeoutError() {
        throw new Error("clearTimeout has not been defined");
    }

    function _0x428e63(_0x2d6b52) {
        if (setTimeoutProxy === setTimeout) return setTimeout(_0x2d6b52, 0);
        if ((setTimeoutProxy === setTimeoutError || !setTimeoutProxy) && setTimeout) return setTimeoutProxy = setTimeout, setTimeout(_0x2d6b52, 0);
        try {
            return setTimeoutProxy(_0x2d6b52, 0);
        } catch (_0x45f485) {
            try {
                return setTimeoutProxy["call"](null, _0x2d6b52, 0);
            } catch (_0x7315ec) {
                return setTimeoutProxy["call"](this, _0x2d6b52, 0);
            }
        }
    }
    
    let setupTimeoutProxies = function() {
        try {
            setTimeoutProxy = "function" == typeof setTimeout ? setTimeout : setTimeoutError;
        } catch (exception) {
            setTimeoutProxy = setTimeoutError;
        }
        try {
            clearTimeoutProxy = "function" == typeof clearTimeout ? clearTimeout : clearTimeoutError;
        } catch (exception) {
            clearTimeoutProxy = clearTimeoutError;
        }
    };
    setupTimeoutProxies();

    var _0x5d9db9, _0x2f100d = [], _0x1cbede = false, _0x13f6f0 = -1;

    function _0x4973b2() {
        _0x1cbede && _0x5d9db9 && (_0x1cbede = false, _0x5d9db9["length"] ? _0x2f100d = _0x5d9db9["concat"](_0x2f100d) : _0x13f6f0 = -0x1, _0x2f100d["length"] && _0x180fd0());
    }

    function _0x180fd0() {
        if (!_0x1cbede) {
            var _0x22af10 = _0x428e63(_0x4973b2);
            _0x1cbede = true;
            for (var _0x159b13 = _0x2f100d["length"]; _0x159b13;) {
                for (_0x5d9db9 = _0x2f100d, _0x2f100d = []; ++_0x13f6f0 < _0x159b13;) _0x5d9db9 && _0x5d9db9[_0x13f6f0]["run"]();
                _0x13f6f0 = -0x1, _0x159b13 = _0x2f100d["length"];
            }
            _0x5d9db9 = null, _0x1cbede = false,
                function(_0x548541) {
                    if (clearTimeoutProxy === clearTimeout) return clearTimeout(_0x548541);
                    if ((clearTimeoutProxy === clearTimeoutError || !clearTimeoutProxy) && clearTimeout) return clearTimeoutProxy = clearTimeout, clearTimeout(_0x548541);
                    try {
                        clearTimeoutProxy(_0x548541);
                    } catch (_0x22a19c) {
                        try {
                            return clearTimeoutProxy['call'](null, _0x548541);
                        } catch (_0x1beabf) {
                            return clearTimeoutProxy["call"](this, _0x548541);
                        }
                    }
                }(_0x22af10);
        }
    }

    function _0x4f1a00(_0x3381a9, _0x4e5f23) {
        this["fun"] = _0x3381a9;
        this["array"] = _0x4e5f23;
    }

    function ProcessClassProxy() {}
    desiredProcessVariable["nextTick"] = function(_0x284e12) {
        var _0x31820c = new Array(arguments["length"] - 1);
        if (arguments['length'] > 1) {
            for (var i = 1; i < arguments["length"]; i++) {
                _0x31820c[i - 1] = arguments[i];
            }
        }

        _0x2f100d["push"](new _0x4f1a00(_0x284e12, _0x31820c));

        if (1 === _0x2f100d["length"] && !!!_0x1cbede) {
            _0x428e63(_0x180fd0);
        }
    };
    _0x4f1a00["prototype"]['run'] = function() {
        this["fun"]["apply"](null, this["array"]);
    };
    desiredProcessVariable["title"] = 'browser';
    desiredProcessVariable["browser"] = true;
    desiredProcessVariable["env"] = {};
    desiredProcessVariable["argv"] = [];
    desiredProcessVariable["version"] = '';
    desiredProcessVariable["versions"] = {};
    desiredProcessVariable['on'] = ProcessClassProxy;
    desiredProcessVariable['addListener'] = ProcessClassProxy;
    desiredProcessVariable['once'] = ProcessClassProxy;
    desiredProcessVariable["off"] = ProcessClassProxy;
    desiredProcessVariable["removeListener"] = ProcessClassProxy;
    desiredProcessVariable["removeAllListeners"] = ProcessClassProxy;
    desiredProcessVariable["emit"] = ProcessClassProxy;
    desiredProcessVariable["prependListener"] = ProcessClassProxy;
    desiredProcessVariable["prependOnceListener"] = ProcessClassProxy;
    desiredProcessVariable["listeners"] = function(_0x119eeb) {
        return [];
    };
    desiredProcessVariable["binding"] = function(_0x538ad0) {
        throw new Error('process.binding is not supported');
    };
    desiredProcessVariable["cwd"] = function() {
        return '/';
    };
    desiredProcessVariable['chdir'] = function(_0x51d0ac) {
        throw new Error("process.chdir is not supported");
    };
    desiredProcessVariable["umask"] = function() {
        return 0;
    };
};

reeseCallParamArray[4] = function(_0x77c3d9, _0x84e5b4, getOrCreateExportsReferenceFunc) {
    var _0x29e309;
    _0x29e309 = function() {
        return this;
    }();
    try {
        _0x29e309 = _0x29e309 || new Function("return this")();
    } catch (_0xd06172) {
        "object" == typeof window && (_0x29e309 = window);
    }
    _0x77c3d9["exports"] = _0x29e309;

    //It is possible, albeit I don't want to get hasty that this entire function is pointless.
    //It looks to just be a _0x77c3d9["exports"] = this, which is weird as that's just a circle as the function is called
    //with `this` bound to _0x77c3d9["exports"]
};

reeseCallParamArray[5] = function(interrogationManagerContainer, interrogationManager, getOrCreateExportsReferenceFunc) {
    'use strict';
    Object["defineProperty"](interrogationManager, "__esModule", {
        'value': true
    });
    var _0x43a779 = getOrCreateExportsReferenceFunc(6);
    interrogationManager["interrogatorFactory"] = function(_0x5c7cf3) {
        return new window['reese84interrogator'](_0x43a779, _0x5c7cf3);
    };
};

reeseCallParamArray[6] = function(globalExportsObject, unusedParam1, getOrCreateExportsReferenceFunc) {
    'use strict';
    var sha1UtilObject = {
        'sha1Encrypt': function(messageToEncrypt) {
            messageToEncrypt = unescape(encodeURIComponent(messageToEncrypt));
            var _0x4ba51f = [1518500249, 1859775393, 2400959708, 3395469782];
            var required32BitLength = (messageToEncrypt += "\u0080")['length'] / 4 + 2;
            var required16BitLength = Math['ceil'](required32BitLength / 16);
            var output16BitArray = new Array(required16BitLength);

            for (let i = 0; i < required16BitLength; i++) {
                output16BitArray[i] = new Array(16);
                for (let innerLoopIterator = 0; innerLoopIterator < 16; innerLoopIterator++) { //Inner loop encodes four letters and jams those into a single int. There's 16 ints per block in the output16BitArray
                    output16BitArray[i][innerLoopIterator] = messageToEncrypt["charCodeAt"](64 * i + 4 * innerLoopIterator) << 24 | messageToEncrypt["charCodeAt"](64 * i + 4 * innerLoopIterator + 1) << 16 | messageToEncrypt["charCodeAt"](64 * i + 0x4 * innerLoopIterator + 2) << 8 | messageToEncrypt['charCodeAt'](64 * i + 4 * innerLoopIterator + 3);
                }
            }

            //Some weirdness to add the length into the last two ints of the array. Yeah, it's a mess but straight from the Sha1 source.
            output16BitArray[required16BitLength - 1][14] = 8 * (messageToEncrypt['length'] - 1) / Math['pow'](2, 32);
            output16BitArray[required16BitLength - 1][14] = Math['floor'](output16BitArray[required16BitLength - 1][14]);
            output16BitArray[required16BitLength - 1][15] = 8 * (messageToEncrypt["length"] - 1) & 4294967295;

            var workingSha1HashVal1, workingSha1HashVal2, workingSha1HashVal3, workingSha1HashVal4, workingSha1HashVal5;
            var sha1HashConst1 = 1732584193,
                sha1HashConst2 = 4023233417,
                sha1HashConst3 = 2562383102,
                sha1HashConst4 = 271733878,
                sha1HashConst5 = 3285377520;
            var bigEndianWords = new Array(80);

            for (let i = 0; i < required16BitLength; i++) {
                //Setup the words.
                for (let innerLoopCounter = 0; innerLoopCounter < 16; innerLoopCounter++) {
                    bigEndianWords[innerLoopCounter] = output16BitArray[i][innerLoopCounter];
                }
                for (let i = 16; i < 80; i++) {
                    bigEndianWords[i] = sha1UtilObject["ROTL"](bigEndianWords[i - 3] ^ bigEndianWords[i - 8] ^ bigEndianWords[i - 14] ^ bigEndianWords[i - 16], 1)
                };

                workingSha1HashVal1 = sha1HashConst1;
                workingSha1HashVal2 = sha1HashConst2;
                workingSha1HashVal3 = sha1HashConst3;
                workingSha1HashVal4 = sha1HashConst4;
                workingSha1HashVal5 = sha1HashConst5;

                for (let i = 0; i < 80; i++) {
                    var _0x565572 = Math["floor"](i / 20);
                    var _0x14b674 = sha1UtilObject["ROTL"](workingSha1HashVal1, 0x5) + sha1UtilObject['f'](_0x565572, workingSha1HashVal2, workingSha1HashVal3, workingSha1HashVal4) + workingSha1HashVal5 + _0x4ba51f[_0x565572] + bigEndianWords[i] & 4294967295;
                    workingSha1HashVal5 = workingSha1HashVal4, workingSha1HashVal4 = workingSha1HashVal3, workingSha1HashVal3 = sha1UtilObject["ROTL"](workingSha1HashVal2, 0x1e), workingSha1HashVal2 = workingSha1HashVal1, workingSha1HashVal1 = _0x14b674;
                }

                //Create an intermediary hash value
                sha1HashConst1 = sha1HashConst1 + workingSha1HashVal1 & 4294967295;
                sha1HashConst2 = sha1HashConst2 + workingSha1HashVal2 & 4294967295;
                sha1HashConst3 = sha1HashConst3 + workingSha1HashVal3 & 4294967295;
                sha1HashConst4 = sha1HashConst4 + workingSha1HashVal4 & 4294967295;
                sha1HashConst5 = sha1HashConst5 + workingSha1HashVal5 & 4294967295;
            }

            //Grab the final hash
            return sha1UtilObject["toHexStr"](sha1HashConst1) + sha1UtilObject["toHexStr"](sha1HashConst2) + sha1UtilObject["toHexStr"](sha1HashConst3) + sha1UtilObject["toHexStr"](sha1HashConst4) + sha1UtilObject["toHexStr"](sha1HashConst5);
        },
        'f': function(_0x5f06ec, _0x17816a, _0x119c97, _0x30dd28) {
            switch (_0x5f06ec) {
                case 0:
                    return _0x17816a & _0x119c97 ^ ~_0x17816a & _0x30dd28;
                case 1: //parity
                    return _0x17816a ^ _0x119c97 ^ _0x30dd28;
                case 2:
                    return _0x17816a & _0x119c97 ^ _0x17816a & _0x30dd28 ^ _0x119c97 & _0x30dd28;
                case 3: //parity
                    return _0x17816a ^ _0x119c97 ^ _0x30dd28;
            }
        },

        'ROTL': function(numberToRotate, rotateByThisManyBits) { //Rotate Left
            return numberToRotate << rotateByThisManyBits | numberToRotate >>> 32 - rotateByThisManyBits;
        },

        'toHexStr': function(numberToConvert) {
            var hexStirng = '';

            for (let i = 7; i >= 0; i--) {
                hexStirng += (numberToConvert >>> 4 * i & 15)["toString"](16);
            }
            return hexStirng;
        }
    };
    if (globalExportsObject['exports']) {
        globalExportsObject["exports"] = sha1UtilObject['sha1Encrypt'];
    }
};

reeseCallParamArray[7] = function(unusedParam1, unusedParam2, getOrCreateExportsReferenceFunc) {
    ! function(httpRequestObject) {
        'use strict';
        if (!httpRequestObject["fetch"]) {
            var requestHasSearchparams = "URLSearchParams" in httpRequestObject;
            var requestHasIterator = 'Symbol' in httpRequestObject && "iterator" in Symbol;
            var isFileReaderOrBlob = false;
            
            if ("FileReader" in httpRequestObject && "Blob" in httpRequestObject) {
               isFileReaderOrBlob = function() {
                    try {
                        new Blob();
                        return true;
                    } catch (UnsupportedExpection) {
                        return false;
                    }
                }();
            }
            var requestHasFormData = "FormData" in httpRequestObject;
            var requestHasArrayBuffer = "ArrayBuffer" in httpRequestObject;
            if (requestHasArrayBuffer) {
                var arrayStringTypes = ['[object Int8Array]', "[object Uint8Array]", "[object Uint8ClampedArray]", '[object Int16Array]', "[object Uint16Array]", "[object Int32Array]", "[object Uint32Array]", "[object Float32Array]", "[object Float64Array]"];
                var _0x3c0b97 = function(_0x130b00) {
                    return _0x130b00 && DataView["prototype"]["isPrototypeOf"](_0x130b00);
                };
                var arrayBufferView = ArrayBuffer["isView"];
                
                if (!!!arrayBufferView) {
                    arrayBufferView = function(arrayBuffer) {
                        if (!!!arrayBuffer) {
                            return false;
                        }

                        return arrayStringTypes["indexOf"](Object['prototype']["toString"]["call"](arrayBuffer)) > -1;
                    };
                }
            }
            headersObject["prototype"]['append'] = function(headerFieldName, headerValue) {
                headerFieldName = makeSafeHeaderField(headerFieldName);
                headerValue = makeString(headerValue);
                var currentHeaderValue = this['map'][headerFieldName];
                this['map'][headerFieldName] = currentHeaderValue ? currentHeaderValue + ',' + headerValue : headerValue;
            };
            headersObject["prototype"]['delete'] = function(headerFieldName) {
                delete this["map"][makeSafeHeaderField(headerFieldName)];
            };
            headersObject["prototype"]["get"] = function(headerFieldName) {
                headerFieldName = makeSafeHeaderField(headerFieldName);
                return this["has"](headerFieldName) ? this["map"][headerFieldName] : null;
            };
            headersObject["prototype"]["has"] = function(headerFieldName) {
                return this["map"]["hasOwnProperty"](makeSafeHeaderField(headerFieldName));
            };
            headersObject["prototype"]["set"] = function(headerFieldName, headerFieldValue) {
                this["map"][makeSafeHeaderField(headerFieldName)] = makeString(headerFieldValue);
            };
            headersObject["prototype"]["forEach"] = function(functionToRunPerHeader, thisObject) {
                for (var headerName in this["map"]) {
                    if (this['map']["hasOwnProperty"](headerName)) {
                        functionToRunPerHeader["call"](thisObject, this["map"][headerName], headerName, this);
                    }
                }
            };
            headersObject["prototype"]["keys"] = function() {
                var keysArray = [];
                this["forEach"](function(unused, key) {
                    keysArray["push"](key);
                });
                
                return getArrayiterator(keysArray);
            };
            headersObject["prototype"]['values'] = function() {
                var valuesArray = [];
                this["forEach"](function(value) {
                    valuesArray["push"](value);
                });
                
                return getArrayiterator(valuesArray);
            };
            headersObject['prototype']['entries'] = function() {
                var entriesArray = [];
                this["forEach"](function(value, key) {
                    entriesArray["push"]([key, value]);
                });
                return getArrayiterator(entriesArray);
            };

            if (requestHasIterator) {
                headersObject["prototype"][Symbol["iterator"]] = headersObject["prototype"]["entries"];
            }
            var httpRequestTypes = ['DELETE', "GET", "HEAD", "OPTIONS", "POST", "PUT"];
            httpEventObject["prototype"]['clone'] = function() {
                return new httpEventObject(this, {
                    'body': this["_bodyInit"]
                });
            };
            
            readResponseBody["call"](httpEventObject["prototype"]);
            readResponseBody["call"](mockedHttpResponse["prototype"]);
            mockedHttpResponse["prototype"]['clone'] = function() {
                return new mockedHttpResponse(this['_bodyInit'], {
                    'status': this['status'],
                    'statusText': this['statusText'],
                    'headers': new headersObject(this["headers"]),
                    'url': this["url"]
                });
            };
            mockedHttpResponse['error'] = function() {
                var httpResponse = new mockedHttpResponse(null, {
                    'status': 0,
                    'statusText': ''
                });

                httpResponse['type'] = "error";
                return httpResponse;
            };
            var validResponseCodes = [301,302,303,307,308];

            mockedHttpResponse["redirect"] = function(redirectLocation, responseCode) {
                if (validResponseCodes['indexOf'](responseCode) === -1) {
                    throw new RangeError("Invalid status code")
                }
                return new mockedHttpResponse(null, {
                    'status': responseCode,
                    'headers': {
                        'location': redirectLocation
                    }
                });
            };
            httpRequestObject["Headers"] = headersObject;
            httpRequestObject["Request"] = httpEventObject;
            httpRequestObject["Response"] = mockedHttpResponse;
            httpRequestObject["fetch"] = function(httpRespnseBody, httpRequestHeaders) {
                return new Promise(function(onSuccessCallback, onErrorCallback) {
                    var httpEvent = new httpEventObject(httpRespnseBody, httpRequestHeaders);
                    var httpRequest = new XMLHttpRequest();
                    httpRequest["onload"] = function() {
                        var responseHeaders, headersObjectRef;
                        responseHeaders = httpRequest["getAllResponseHeaders"]() || '';
                        headersObjectRef = new headersObject();
                        responseHeaders["replace"](/\r?\n[\t ]+/g, ' ')["split"](/\r?\n/)['forEach'](function(headerLine) {
                            let headerSplit = headerLine["split"](':');
                            let headerName = headerSplit['shift']()['trim']();
                            if (headerName) {
                                headersObjectRef["append"](headerName, headerSplit.join(":").trim());
                            }
                        });
                        var requestResponseHeaderData = {
                            'status': httpRequest['status'],
                            'statusText': httpRequest["statusText"],
                            'headers': headersObjectRef,
                        };

                        requestResponseHeaderData["url"] = "responseURL" in httpRequest ? httpRequest["responseURL"] : requestResponseHeaderData["headers"]["get"]("X-Request-URL");
                        var httpResponseBody = "response" in httpRequest ? httpRequest['response'] : httpRequest["responseText"];
                        onSuccessCallback(new mockedHttpResponse(httpResponseBody, requestResponseHeaderData));
                    };
                    httpRequest['onerror'] = function() {
                        onErrorCallback(new TypeError("Network request failed"));
                    };
                    httpRequest["ontimeout"] = function() {
                        onErrorCallback(new TypeError("Network request failed"));
                    };
                    httpRequest['open'](httpEvent["method"], httpEvent["url"], true);

                    if ("include" === httpEvent["credentials"]) {
                        httpRequest["withCredentials"] = true;
                    } else if ('omit' === httpEvent["credentials"]) {
                        (httpRequest["withCredentials"] = false)
                     }
                     
                     if ('responseType' in httpRequest && isFileReaderOrBlob) {
                         httpRequest["responseType"] = "blob";
                     }
                    
                    httpEvent["headers"]["forEach"](function(headerName, headerValue) {
                        httpRequest["setRequestHeader"](headerValue, headerName);
                    });
                    httpRequest['send'](httpEvent['_bodyInit'] === undefined ? null : httpEvent["_bodyInit"]);
                });
            };
            httpRequestObject["fetch"]["polyfill"] = true;
        }

        function makeSafeHeaderField(headerFieldName) {
            if ("string" != typeof headerFieldName) {
                headerFieldName = String(headerFieldName);
            }
            
            let regexExpression = /[^a-z0-9\-#$%&'*+.\^_`|~]/i;
            if (regexExpression['test'](headerFieldName)) {
                throw new TypeError("Invalid character in header field name");
            }
            return headerFieldName["toLowerCase"]();
        }

        function makeString(param) {
            if ("string" != typeof param) {
                param = String(param);
             }
             
             return param;
        }

        function getArrayiterator(someArray) {
            var iterator = {
                'next': function() {
                    var topElement = someArray['shift']();
                    return {
                        'done': undefined === topElement,
                        'value': topElement
                    };
                }
            };

            if (requestHasIterator) {
                iterator[Symbol['iterator']] = function() {
                    return iterator;
                }
            }
            return iterator;
        }

        function headersObject(incomingHeaders) {
            this["map"] = {};
            if (!!!incomingHeaders) {
                return;
            }

            let forEachFunction;
            if (incomingHeaders instanceof headersObject) {
                forEachFunction = function(headerValue, headerName) {
                    this['append'](headerName, headerValue);
                };
            } else if (Array.isArray(incomingHeaders)) {
                forEachFunction = function(headerData) {
                    this["append"](headerData[0], headerData[1]);
                };
            } else {
                forEachFunction = function(headerName) {
                    this['append'](headerName, incomingHeaders[headerName]);
                }
                Object["getOwnPropertyNames"](incomingHeaders)["forEach"](forEachFunction, this);
                return;
            }

            incomingHeaders['forEach'](forEachFunction, this);
        }

        function setBodyUsed(bodyObject) {
            if (bodyObject["bodyUsed"]) {
                return Promise["reject"](new TypeError("Already read"));
            }
            bodyObject["bodyUsed"] = true;
        }

        function getLoadPromise(callbackParamHolder) {
            return new Promise(function(onLoadCallback, onErrorCallback) {
                callbackParamHolder["onload"] = function() {
                    onLoadCallback(callbackParamHolder["result"]);
                };
                callbackParamHolder['onerror'] = function() {
                    onErrorCallback(callbackParamHolder["error"]);
                };
            });
        }

        function readData(incomingData) {
            var fileReader = new FileReader();
            var fileLoadPromise = getLoadPromise(fileReader);
            fileReader["readAsArrayBuffer"](incomingData);
            
            return fileLoadPromise;
        }

        function getArrayBuffer(incomingArray) {
            if (incomingArray["slice"]) {
                return incomingArray["slice"](0);
            }
            var unsignedEightBitArray = new Uint8Array(incomingArray['byteLength']);
            unsignedEightBitArray['set'](new Uint8Array(incomingArray));
            
            return unsignedEightBitArray["buffer"];
        }

        function readResponseBody() {
            this["bodyUsed"] = false;
            this["_initBody"] = function(initialBodyValue) {
                this['_bodyInit'] = initialBodyValue;
                if (initialBodyValue) {
                    if ('string' == typeof initialBodyValue) {
                        this["_bodyText"] = initialBodyValue;
                    } else if (isFileReaderOrBlob && Blob["prototype"]['isPrototypeOf'](initialBodyValue)) {
                        this["_bodyBlob"] = initialBodyValue;
                    } else if (requestHasFormData && FormData["prototype"]["isPrototypeOf"](initialBodyValue)) {
                        this["_bodyFormData"] = initialBodyValue;
                    } else if (requestHasSearchparams && URLSearchParams["prototype"]["isPrototypeOf"](initialBodyValue)) {
                        this["_bodyText"] = initialBodyValue["toString"]();
                    } else if (requestHasArrayBuffer && isFileReaderOrBlob && _0x3c0b97(initialBodyValue)) {
                        this['_bodyArrayBuffer'] = getArrayBuffer(initialBodyValue["buffer"]);
                        this["_bodyInit"] = new Blob([this["_bodyArrayBuffer"]]);
                    } else {
                        if (!requestHasArrayBuffer || !ArrayBuffer["prototype"]['isPrototypeOf'](initialBodyValue) && !arrayBufferView(initialBodyValue)) {
                            throw new Error("unsupported BodyInit type");
                        }
                        this['_bodyArrayBuffer'] = getArrayBuffer(initialBodyValue);
                    }
                } else {
                    this["_bodyText"] = '';
                }

                if (!!!this["headers"]["get"]("content-type")) {
                    if ("string" == typeof initialBodyValue) {
                        this["headers"]["set"]("content-type", "text/plain;charset=UTF-8")
                    } else if (this["_bodyBlob"] && this["_bodyBlob"]["type"]) {
                        this["headers"]['set']('content-type', this["_bodyBlob"]["type"]);
                    } else if (requestHasSearchparams && URLSearchParams["prototype"]["isPrototypeOf"](initialBodyValue)) {
                        this["headers"]["set"]("content-type", "application/x-www-form-urlencoded;charset=UTF-8");
                    }
                }
            }

            if (isFileReaderOrBlob) {
                this["blob"] = function() {
                    let bodyUsedPromise = setBodyUsed(this);
                    if (bodyUsedPromise) {
                        return bodyUsedPromise;
                    }
                    if (this["_bodyBlob"]) {
                        return Promise["resolve"](this["_bodyBlob"]);
                    }
                    if (this['_bodyArrayBuffer']) {
                        return Promise["resolve"](new Blob([this["_bodyArrayBuffer"]]));
                    }
                    if (this["_bodyFormData"]) {
                        throw new Error("could not read FormData body as blob");
                    }
                    return Promise["resolve"](new Blob([this["_bodyText"]]));
                };
                this['arrayBuffer'] = function() {
                    if (this["_bodyArrayBuffer"]) {
                        let bodyUsedPromise = setBodyUsed(this);
                        if (bodyUsedPromise) {
                            return bodyUsedPromise;
                        }

                        return Promise["resolve"](this['_bodyArrayBuffer']);
                    }
                    return this["blob"]()["then"](readData);
                }
            }

            this["text"] = function() {
                let bodyUsedPromise = setBodyUsed(this);
                if (bodyUsedPromise) {
                    return bodyUsedPromise;
                }

                if (this['_bodyBlob']) {
                    let bodyBlob = this["_bodyBlob"];
                    let fileReader = new FileReader();
                    let loadPromise = getLoadPromise(fileReader);
                    fileReader["readAsText"](bodyBlob);
                    return loadPromise;
                }
                if (this["_bodyArrayBuffer"]) {
                    let arrayBufferPromiseCallback = function(arrayBuffer) {
                        let charCodes = new Uint8Array(arrayBuffer);
                        let resolvedCharacters = new Array(charCodes["length"])
                        for (let i = 0; i < charCodes["length"]; i++) {
                            resolvedCharacters[i] = String['fromCharCode'](charCodes[i]);
                        }
                        return resolvedCharacters["join"]('');
                    };

                    let arrayBufferPromise = Promise["resolve"](arrayBufferPromiseCallback);
                    return arrayBufferPromise(this["_bodyArrayBuffer"]);
                }
                if (this['_bodyFormData']) {
                    throw new Error("could not read FormData body as text");
                }

                return Promise["resolve"](this["_bodyText"]);
            }

            if (requestHasFormData) {
                this['formData'] = function() {
                    return this["text"]()["then"](readTextCallback);
                };
            }

            this["json"] = function() {
                return this.text().then(JSON.parse);
            }

            return this;
        }

        function httpEventObject(responseBody, request) {
            var requestMethod, upperCaseRequestMethod;
            var requestBody = undefined;
            if (!!request) {
                requestBody = request["body"];
            }

            if (responseBody instanceof httpEventObject) {
                if (responseBody['bodyUsed']) {
                    throw new TypeError("Already read");
                }
                this["url"] = responseBody["url"];
                this["credentials"] = responseBody['credentials'];
                if (!!!request["headers"]) {
                    this["headers"] = new headersObject(responseBody["headers"]);
                }
                this['method'] = responseBody["method"];
                this['mode'] = responseBody["mode"];
                if (!!!requestBody && responseBody["_bodyInit"] != null) {
                    requestBody = responseBody["_bodyInit"];
                    responseBody["bodyUsed"] = true;
                }
                 
            } else {
                this['url'] = String(responseBody);
            }

            this["credentials"] = request["credentials"] || this['credentials'] || "omit";
            if (request["headers"] && !!!this["headers"]) {
                this["headers"] = new headersObject(request["headers"])
            }

            requestMethod = request["method"] || this["method"] || "GET";

            upperCaseRequestMethod = requestMethod["toUpperCase"]()

            this["method"] = httpRequestTypes["indexOf"](upperCaseRequestMethod) > -1 ? upperCaseRequestMethod : requestMethod;
            this['mode'] = request['mode'] || this["mode"] || null;
            
            this["referrer"] = null;

            if (requestBody) {
                if ('GET' === this["method"] || 'HEAD' === this["method"]) {
                    throw new TypeError("Body not allowed for GET or HEAD requests");
                }
            }

            this["_initBody"](requestBody);
        }

        function readTextCallback(unformattedFormData) {
            var formData = new FormData();
            return unformattedFormData["trim"]()["split"]('&')["forEach"](function(formDataPiece) {
                if (formDataPiece) {
                    let formDataSplit = formDataPiece["split"]('=');
                    let formDataName = formDataSplit["shift"]()["replace"](/\+/g, ' ');
                    let formDataValue = formDataSplit["join"]('=')["replace"](/\+/g, ' ');
                    formData['append'](decodeURIComponent(formDataName), decodeURIComponent(formDataValue));
                }
            }), formData;
        }

        function mockedHttpResponse(httpResponseBody, httpResponseInfo) {
            if (!!!httpResponseInfo) {
                httpResponseInfo = {};
            }

            this["type"] = "default";
            this["status"] = !!httpResponseInfo["status"] ?  httpResponseInfo["status"] : 200;            
            this['ok'] = this["status"] >= 200 && this["status"] < 300;
            this["statusText"] = 'statusText' in httpResponseInfo ? httpResponseInfo["statusText"] : 'OK';
            this["headers"] = new headersObject(httpResponseInfo["headers"]);
            this['url'] = httpResponseInfo['url'] || '';
            this['_initBody'](httpResponseBody);
        }
    }('undefined' != typeof self ? self : this);
};

reeseCallParamArray[8] = function(automationCheckContainer, automationCheckObject, getOrCreateExportsReferenceFunc) {
    'use strict';
    Object["defineProperty"](automationCheckObject, '__esModule', {
        'value': true
    });
    
    automationCheckObject['automationCheck'] = function(onAutomationDetectedCallback) {
        var timerId = false, automationCheckCount = 2;

        var seleniumDetectionCallback = function _0x2511c8() {
            timerId = setTimeout(_0x2511c8, 200 * automationCheckCount++);
            var seleniumDetectionCode = 0,
                i = null,
                browserEvalCall = null,
                automatedBrowserEvalCalls = ["__driver_evaluate", "__webdriver_evaluate", ​"__selenium_evaluate", ​"__fxdriver_evaluate", ​"__driver_unwrapped", ​"__webdriver_unwrapped", ​"__selenium_unwrapped", ​"__fxdriver_unwrapped", ​"__webdriver_script_function", ​"__webdriver_script_func", "__webdriver_script_fn"],
                automatedBrowserNames = ["_Selenium_IDE_Recorder", "_phantom", "_selenium", "callPhantom", "callSelenium", "__nightmare"];
            try {
                for (let i = 0; i < automatedBrowserNames.length; i++) {
                    let browserName = automatedBrowserNames[i];
                    if (window[browserName]) {
                        seleniumDetectionCode = 100 + parseInt(i);
                    }
                }
                for (let i = 0; i < automatedBrowserEvalCalls.length; i++) {
                    browserEvalCall = automatedBrowserEvalCalls[i];
                    if (window["document"][browserEvalCall]) {
                        seleniumDetectionCode = 200 + parseInt(i);
                    }
                }
                for (let documentElement in window["document"]) {
                    if (documentElement["match"](/\$[a-z]dc_/) && window["document"][documentElement]["cache_"]) {
                            seleniumDetectionCode = '300';
                    }
                }
            } catch (ignoredException) {}
            try {
                if (!seleniumDetectionCode  && window["external"] && window["external"]["toString"]() && window["external"]['toString']()["indexOf"]("Sequentum") != -1) {
                    seleniumDetectionCode = "400";
                }
            } catch (ignoredException) {}
            try {
                if (!seleniumDetectionCode && window["document"]['documentElement']["getAttribute"]('selenium')) {
                    seleniumDetectionCode = '500';
                } else if (!seleniumDetectionCode && window["document"]["documentElement"]["getAttribute"]("webdriver")) {
                    seleniumDetectionCode = '600';
                } else if (!seleniumDetectionCode && window["document"]["documentElement"]["getAttribute"]("driver")) {
                    seleniumDetectionCode = "700";
                }
            } catch (ignoredException) {}
            if (seleniumDetectionCode) {
                onAutomationDetectedCallback('d=' + seleniumDetectionCode);
                clearInterval(timerId);
                try {
                    if (window['location']["hostname"]) {
                        var editedHostname = window["location"]['hostname']['replace'](/\./g, '_') + "___dTL";
                        if (document['getElementById'](editedHostname) && "INPUT" == document["getElementById"](editedHostname)["nodeName"]) {
                            document["getElementById"](editedHostname)['value'] = seleniumDetectionCode
                        }
                    }
                } catch (ignoredException) {}
            }
        };

        window["document"]["addEventListener"]("driver-evaluate", seleniumDetectionCallback, false);
        window["document"]["addEventListener"]("webdriver-evaluate", seleniumDetectionCallback, false);
        window["document"]["addEventListener"]('selenium-evaluate', seleniumDetectionCallback, false);
        seleniumDetectionCallback();
    };
};

reeseCallParamArray[9] = function(logNoopContainer, logNoopObject, getOrCreateExportsReferenceFunc) {
    'use strict';
    logNoopObject["__esModule"] = true;
    logNoopObject['log'] = function(thingToLog) {};
};

reeseCallParamArray[10] = function(schedulerContainer, schedulerObject, getOrCreateExportsReferenceFunc) {
    'use strict';
    var awaiter, responseHandlerGenerator;
    if (this && this["__awaiter"]) {
        awaiter = this["__awaiter"];
    } else {
        awaiter = function(applyFunctionThisReference, appliableFunction) {
            return new Promise(function(doneCallback, _0x2b60e5) {
                function thenCallback(thenResult) {
                    try {
                        handlePromise(appliableFunction["next"](thenResult));
                    } catch (exception) {
                        _0x2b60e5(exception);
                    }
                }

                function thenErrorback(thenError) {
                    try {
                        handlePromise(appliableFunction["throw"](thenError));
                    } catch (exception) {
                        _0x2b60e5(exception);
                    }
                }

                function handlePromise(promiseWrapperObject) {
                    if (promiseWrapperObject["done"]) {
                        doneCallback(promiseWrapperObject["value"])
                    } else {
                        let promiseToCall = promiseWrapperObject["value"]
                        if (!(promiseToCall instanceof Promise)) {
                            promiseToCall = new Promise(
                                function(onPromiseResolutionCallback) {
                                    onPromiseResolutionCallback(promiseToCall);
                                }
                            );
                        }

                        promiseToCall["then"](thenCallback, thenErrorback);
                    }
                }

                appliableFunction = appliableFunction['apply'](applyFunctionThisReference, []);

                handlePromise(appliableFunction["next"]());
            });
        };
    }

    if (this && this['__generator']) {
        responseHandlerGenerator = this['__generator'];
    } else {
        responseHandlerGenerator = function(callFunctionThis, callbackFunction) {
            var isGeneratorExecuting, internalCallbackThis, internalCallback;
            var callbackArguments = {
                'label': 0,
                'sent': function() {
                    if (1 & internalCallback[0]) {
                        throw internalCallback[1];
                    }
                    return internalCallback[1];
                },
                'trys': [],
                'ops': []
            };

            function generateResponseCodeHandler(curriedResponseCode) {
                return function(currentResponseCode) {
                    return function(responseCodeArray) {
                        if (isGeneratorExecuting) {
                            throw new TypeError('Generator is already executing.');
                        }

                        while (callbackArguments) {
                            try {
                                isGeneratorExecuting = 1;

                                if (internalCallbackThis) {
                                    if (2 & responseCodeArray[0]) {
                                        internalCallback = internalCallbackThis["return"];
                                    } else if (responseCodeArray[0]) {
                                        if (internalCallbackThis["throw"]) {
                                            internalCallback = internalCallbackThis["throw"];
                                        } else {
                                            internalCallback = internalCallbackThis["return"];
                                            if (internalCallback) {
                                                internalCallback["call"](internalCallbackThis);
                                            }
                                            internalCallback = 0;
                                        }
                                    } else {
                                        internalCallback = internalCallbackThis["next"];
                                    }

                                    if (internalCallback) {
                                        internalCallback = internalCallback["call"](internalCallbackThis, responseCodeArray[1]);
                                        if (!internalCallback['done']) {
                                            return internalCallback;
                                        }
                                    }
                                }

                                internalCallbackThis = 0;

                                if (internalCallback) {
                                    responseCodeArray = [2 & responseCodeArray[0], internalCallback["value"]];
                                }


                                switch (responseCodeArray[0]) {
                                    case 0:
                                    case 1:
                                        internalCallback = responseCodeArray;
                                        break;
                                    case 4:
                                        callbackArguments.label++;
                                        return {
                                            'value': responseCodeArray[1],
                                            'done': false
                                        };
                                    case 5:
                                        callbackArguments.label++;
                                        internalCallbackThis = responseCodeArray[1];
                                        responseCodeArray = [0];
                                        continue;
                                    case 7:
                                        callbackArguments['trys']["pop"]();
                                        responseCodeArray = callbackArguments["ops"]["pop"]();
                                        continue;
                                    default:
                                        internalCallback = callbackArguments['trys']
                                        if (!((internalCallback = internalCallback['length'] > 0 && internalCallback[internalCallback["length"] - 1]) || (6 !== responseCodeArray[0] && 2 !== responseCodeArray[0]))) {
                                            callbackArguments = 0;
                                            continue;
                                        }
                                        if (3 === responseCodeArray[0] && (!internalCallback || responseCodeArray[1] > internalCallback[0] && responseCodeArray[1] < internalCallback[3])) {
                                            callbackArguments.label = responseCodeArray[1];
                                            break;
                                        }
                                        if (6 === responseCodeArray[0] && callbackArguments.label < internalCallback[1]) {
                                            callbackArguments.label = internalCallback[1];
                                            internalCallback = responseCodeArray;
                                            break;
                                        }
                                        if (internalCallback && callbackArguments.label < internalCallback[2]) {
                                            callbackArguments.label = internalCallback[2];
                                            callbackArguments["ops"]["push"](responseCodeArray);
                                            break;
                                        }

                                        if (internalCallback[2]) {
                                            callbackArguments['ops']["pop"]();
                                        }
                                        
                                        callbackArguments['trys']["pop"]();
                                        continue;
                                }
                                responseCodeArray = callbackFunction['call'](callFunctionThis, callbackArguments);
                            } catch (exception) {
                                responseCodeArray = [6, exception];
                                internalCallbackThis = 0;
                            } finally {
                                isGeneratorExecuting = internalCallback = 0;
                            }
                        }
                        if (5 & responseCodeArray[0]) {
                            throw responseCodeArray[1];
                        }
                        return {
                            'value': responseCodeArray[0] ? responseCodeArray[1] : undefined,
                            'done': true
                        };
                    }([curriedResponseCode, currentResponseCode]);
                };
            }

            var responseCodeHandler = {
                'next': generateResponseCodeHandler(0),
                'throw': generateResponseCodeHandler(1),
                'return': generateResponseCodeHandler(2)
            };
            if ("function" == typeof Symbol) {
                responseCodeHandler[Symbol['iterator']] = function() {
                    return this;
                };
            }
            return responseCodeHandler;
        };
    }
    schedulerObject["__esModule"] = true;
    var robustScheduler = function() {
        function internalRobustScheduler() {
            var thisReference = this;
            this["callback"] = undefined;
            this["triggerTimeMs"] = undefined;
            this["timerId"] = undefined;
            document["addEventListener"]("online", function() {
                return thisReference["update"]();
            });
            document["addEventListener"]("pageshow", function() {
                return thisReference['update']();
            });
            document["addEventListener"]("visibilitychange", function() {
                return thisReference['update']();
            });
        };

        internalRobustScheduler['prototype']["runLater"] = function(callbackFunction, msUntilCallback) {
            let thisReference = this;
            this["stop"]();

            if (msUntilCallback <= 0) {
                callbackFunction();
            } else {
                var currentTime = new Date()["getTime"]();
                var fencedMsUntilCallback = Math["min"](10000, msUntilCallback);
                this["callback"] = callbackFunction;
                this['triggerTimeMs'] = currentTime + msUntilCallback;
                this['timerId'] = window['setTimeout'](function() {
                    return thisReference["onTimeout"](currentTime + fencedMsUntilCallback);
                }, fencedMsUntilCallback);
            }
        };
        
        internalRobustScheduler["prototype"]["stop"] = function() {
            window['clearTimeout'](this["timerId"]);
            this["callback"] = undefined;
            this["triggerTimeMs"] = undefined;
            this["timerId"] = undefined;
        };
        
        internalRobustScheduler['prototype']["onTimeout"] = function(timeoutTime) {
            if (this['callback']) {
                if (new Date()["getTime"]() < timeoutTime - 100) {
                    this['fire']();
                } else {
                    this['update']();
                }
            }
        };
        
        internalRobustScheduler['prototype']["update"] = function() {
            let thisReference = this;
            if (this['callback'] && this["triggerTimeMs"]) {
                var currentTime = new Date()["getTime"]();
                if (this["triggerTimeMs"] < currentTime + 100) {
                    this["fire"]();
                } else {
                    window['clearTimeout'](this["timerId"]);
                    var msUntilTrigger = this["triggerTimeMs"] - currentTime;
                    var fencedMsUntilTrigger = Math["min"](10000, msUntilTrigger);

                    this["timerId"] = window["setTimeout"](function() {
                        return thisReference["onTimeout"](currentTime + fencedMsUntilTrigger);
                    }, fencedMsUntilTrigger);
                }
            }
        };
        
        internalRobustScheduler["prototype"]['fire'] = function() {
            if (this["callback"]) {
                var callbackReference = this["callback"];
                this["stop"]();
                callbackReference();
            }
        };
        
        return internalRobustScheduler;
    }();

    function createPromiseOf(promiseOfRobustScheduler, msUntilCallback) {
        return new Promise(
            function(onSuccessCallback) {
                promiseOfRobustScheduler["runLater"](onSuccessCallback, msUntilCallback);
            }
        );
    }
    schedulerObject["RobustScheduler"] = robustScheduler;
    schedulerObject["retry"] = function(callbackFunction, _0x45bcaf, _0x29836a) {
        return awaiter(this, function() {
            var retryCounter, _0x24f5bb, msUntilCallbackFires;
            return responseHandlerGenerator(this, function(callbackResponseObj) {
                switch (callbackResponseObj.label) {
                    case 0:
                        retryCounter = 0;
                        callbackResponseObj.label = 1;
                    case 1:
                        callbackResponseObj['trys']['push']([1, 3, undefined, 7]);
                        return [4, _0x45bcaf()];
                    case 2:
                        return [2, callbackResponseObj['sent']()];
                    case 3:
                        _0x24f5bb = callbackResponseObj['sent']();
                        
                        if (_0x29836a(_0x24f5bb)) {
                            msUntilCallbackFires = 1000 * Math["pow"](1.618, retryCounter + Math["random"]());
                            return [4, createPromiseOf(callbackFunction, msUntilCallbackFires)];
                        } else {
                            return [3, 5];
                        }
                    case 4:
                        return callbackResponseObj["sent"](), [0x3, 0x6];
                    case 5:
                        throw _0x24f5bb;
                    case 6:
                        return [3, 7];
                    case 7:
                        ++retryCounter;
                        return [3, 1];
                    case 8:
                        return [2];
                }
            });
        });
    };
};

reeseCallParamArray[11] = function(performanceContainer, performanceObject, getOrCreateExportsReferenceFunc) {
    'use strict';
    performanceObject["__esModule"] = true;
    performanceObject["timerFactory"] = function() {
        var hasReesePerformance = location["search"]["indexOf"]("reese84_performance") !== -1;
        return performance && hasReesePerformance ? new reese84Performance() : new DateBasedTimer();
    };
    var reese84Performance = function() {
        function performanceConstructor(isFullEnabled) {
            this['enableFull'] = isFullEnabled;
        }
        performanceConstructor["prototype"]["start"] = function(reeseInstanceName) {
            this["mark"]("reese84_" + reeseInstanceName + "_start");
        };
        
        performanceConstructor["prototype"]['startInternal'] = function(reeseInstanceName) {
            if (this["enableFull"]) {
                this["start"](reeseInstanceName);
            }
        };
        
        performanceConstructor['prototype']['stop'] = function(reeseInstanceName) {
            reeseInstanceName = 'reese84_' + reeseInstanceName;
            var reese84stop = reeseInstanceName + "_stop";
            this["mark"](reese84stop);
            performance['clearMeasures'](reeseInstanceName);
            performance["measure"](reeseInstanceName, reeseInstanceName + "_start", reese84stop);
        };
        
        performanceConstructor["prototype"]['stopInternal'] = function(reeseInstanceName) {
            if (this["enableFull"]) {
                this["stop"](reeseInstanceName)
            };
        };
        
        performanceConstructor["prototype"]["summary"] = function() {
            return performance.getEntriesByType("measure")
                .filter(currentEntry => currentEntry.name.indexOf("reese84") === 0)
                .reduce((accumulationObject, currentValue) => 
                    {
                        let cleanedName = currentValue.name.replace("reese84_", "");
                        accumulationObject[cleanedName] = currentValue["duration"];
                        return accumulationObject;
                    }, {}
                );
        };
        
        performanceConstructor["prototype"]["mark"] = function(timestampName) {
            performance['clearMarks'](timestampName);
            performance['mark'](timestampName);
        };
        
        return performanceConstructor;
    }();

    function getCurrentTime() {
        return Date["now"] ? Date["now"]() : new Date()['getTime']();
    }
    performanceObject["PerformanceTimer"] = reese84Performance;
    var DateBasedTimer = function() {
        function dateTimer() {
            this["marks"] = {};
            this["measures"] = {};
        }
        dateTimer["prototype"]['start'] = function(nameOfTimer) {
            this['marks'][nameOfTimer] = getCurrentTime();
        };
        dateTimer['prototype']["startInternal"] = function(unusedParam) {};
        
        dateTimer['prototype']['stop'] = function(nameOfTimer) {
            this["measures"][nameOfTimer] = getCurrentTime() - this["marks"][nameOfTimer];
        };
        dateTimer['prototype']["stopInternal"] = function(unusedParam) {};
        
        dateTimer['prototype']['summary'] = function() {
            return this["measures"];
        };
        
        return dateTimer;
    }();
    performanceObject["DateTimer"] = DateBasedTimer;
};

reeseCallParamArray[12] = undefined;

reeseCallParamArray[13] = function(protectionObjectContainer, protectionObject, getOrCreateExportsRefFunction) {
    'use strict';
    protectionObject['__esModule'] = true;
    let protectionInitializer = function(existingProtectionObject) {
        for (var protectionProperty in existingProtectionObject) {
            if (!protectionObject['hasOwnProperty'](protectionProperty)) {
                protectionObject[protectionProperty] = existingProtectionObject[protectionProperty];
            }
        }
    };

    protectionInitializer(getOrCreateExportsRefFunction(1));
    var existingProtectionObject = getOrCreateExportsRefFunction(1);
    var localCookieManager = getOrCreateExportsRefFunction(0);
    var challengeScript = null;

    function initializeProtectionFunc() {
        var incapsulaProtection = new existingProtectionObject["Protection"]();
        var initializationErrorback = function() {
            if (!challengeScript) {
                challengeScript = localCookieManager["findChallengeScript"]();
            }

            if (challengeScript || challengeScript['parentNode']) {
                window['reeseRetriedAutoload'] = true;
                var challengeScriptParent = challengeScript['parentNode'];
                challengeScriptParent['removeChild'](challengeScript);

                var reeseScriptElement = document["createElement"]("script");
                reeseScriptElement["src"] = challengeScript["src"] + "?cachebuster=" + new Date()["toString"]();
                challengeScriptParent["appendChild"](reeseScriptElement);
                challengeScript = reeseScriptElement;
            }
        };

        if (window["reeseRetriedAutoload"]) {
            initializationErrorback = function(error) {
                console["error"]('Reloading the challenge script failed. Shutting down.', error["toString"]());
            }
        }
        incapsulaProtection["start"](window["reeseSkipExpirationCheck"]);
        
        incapsulaProtection["token"](1000000)["then"](function() {
            return localCookieManager['callGlobalCallback']("onProtectionInitialized", incapsulaProtection);
        }, initializationErrorback);
        
        window["protectionSubmitCaptcha"] = function(captchaProvider, captchaPayload, captchaTimeout, captchaData) {
            return incapsulaProtection['submitCaptcha'](captchaProvider, captchaPayload, captchaTimeout, captchaData);
        };
    }
    protectionObject["initializeProtection"] = initializeProtectionFunc;
    window["initializeProtection"] = initializeProtectionFunc;

    let skipAutoload = window['reeseSkipAutoLoad'];

    if (!!!skipAutoload) {
        skipAutoload = function() {
            try {
                return "true" === localCookieManager["findChallengeScript"]()["getAttribute"]("data-advanced");
            } catch (exception) {
                return false;
            }
        }();
    }

    if (skipAutoload) {
        setTimeout(function() {
            return localCookieManager["callGlobalCallback"]("onProtectionLoaded");
        }, 0);
    } else {
        initializeProtectionFunc();
    }
};

function runBrowserInterrogation(Mj, performanceTimer) {
    this["interrogate"] = function(onSuccessCallback, onErrorCallback) {
        try {
            var iframe = documentReference["createElement"]("IFRAME");
            iframe["style"]["display"] = "none";
            iframe["addEventListener"]("load", function() {
                try {
                    performanceTimer["start"]("interrogation");
                    var randomInteger = window["Math"]["random"]() * 1073741824 | 0;
                    var iframeContentWindow = iframe["contentWindow"];
                    var iframeContentNavigator = iframeContentWindow["navigator"];
                    var iframeContentDocument = iframe["contentDocument"];
                    var canvasDataURL = null;
                    var canvasDataHash = null;
                    var canvasInfoObject = {};
                    var webGlContext = null;
                    var webGlCanvasDataURL = null;
                    var webGlCanvasDataHash = null;
                    var webGlInfoObject = null;
                    var iframeInformationObject = {};
                    var fingerprintingFunctions = [];
                    fingerprintingFunctions["push"](function() {
                        iframeInformationObject["user_agent"] = iframeContentNavigator["userAgent"];
                        iframeInformationObject["language"] = iframeContentNavigator["language"];

                        var languagesPropDescriptor = {};
                        try {
                            languagesPropDescriptor["property_descriptor"] = window["Object"]["getOwnPropertyDescriptor"](iframeContentNavigator, "languages") !== undefined;
                        } catch (exception) {}
                        iframeInformationObject["languages"] = languagesPropDescriptor;

                        var randomNumberGenerator_1 = randomNumberGeneratorFactory(612538604, randomInteger);
                        var random255MaxNumbers_1 = [];
                        for (let i = 0; i < 20; i++) {
                            random255MaxNumbers_1.push(randomNumberGenerator_1() & 255);
                        }

                        var screenInfoObject = {};
                        screenInfoObject["width"] = window["screen"]["width"];
                        screenInfoObject["height"] = window["screen"]["height"];
                        if (window["screen"]["availHeight"] !== undefined) {
                            screenInfoObject["avail_height"] = window["screen"]["availHeight"];
                        }
                        if (window["screen"]["availLeft"] !== undefined) {
                            screenInfoObject["avail_left"] = window["screen"]["availLeft"];
                        }
                        if (window["screen"]["availTop"] !== undefined) {
                            screenInfoObject["avail_top"] = window["screen"]["availTop"];
                        }
                        if (window["screen"]["availWidth"] !== undefined) {
                            screenInfoObject["avail_width"] = window["screen"]["availWidth"];
                        }
                        if (window["screen"]["pixelDepth"] !== undefined) {
                            screenInfoObject["pixel_depth"] = window["screen"]["pixelDepth"];
                        }
                        if (window["innerWidth"] !== undefined) {
                            screenInfoObject["inner_width"] = window["innerWidth"];
                        }
                        if (window["innerHeight"] !== undefined) {
                            screenInfoObject["inner_height"] = window["innerHeight"];
                        }
                        try {
                            if (window["outerWidth"] !== undefined) {
                                screenInfoObject["outer_width"] = window["outerWidth"];
                            }
                        } catch (exception) {}
                        try {
                            if (window["outerHeight"] !== undefined) {
                                screenInfoObject["outer_height"] = window["outerHeight"];
                            }
                        } catch (exception) {}
                        if (window["devicePixelRatio"] !== undefined) {
                            screenInfoObject["device_pixel_ratio"] = window["devicePixelRatio"];
                        }
                        var screenInfoString = window.JSON.stringify(screenInfoObject, jsonStringUndefinedReplace);

                        function safeScreenInfoFix(infoString, randomNumbersArray, randomNumberIndexCeiling) {
                            let safeInfoString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);
                            let characterArray = safeInfoString.split("").reverse();
                            let delimitedCharArray = characterArray.reduce(function(result, currentValue, currentIndex) {
                                result.push(currentValue);

                                let randomNumberIndex = (characterArray.length - 1 - currentIndex) % randomNumberIndexCeiling;
                                result.push(String.fromCharCode(randomNumbersArray[randomNumberIndex]));

                                return result;
                            }, []);

                            return window.btoa(delimitedCharArray.join(""));

                            //Remove unsafe unicode
                            //Convert string to char array
                            //Reverse the array
                            //Insert a random character (based on the random numbers array) after every character)
                            //Convert to base64
                            //return the base64 string
                        }

                        iframeInformationObject["screen"] = safeScreenInfoFix(screenInfoString, random255MaxNumbers_1, 19);

                        iframeInformationObject["timezone"] = new window["Date"]()["getTimezoneOffset"]() / -60;
                        iframeInformationObject["indexed_db"] = iframeContentWindow["indexedDB"] ? true : false;
                        iframeInformationObject["add_behavior"] = iframeContentDocument["body"]["addBehavior"] ? true : false; //Should only be true in IE
                        iframeInformationObject["open_database"] = iframeContentWindow["openDatabase"] ? true : false;
                        iframeInformationObject["cpu_class"] = iframeContentNavigator["cpuClass"] || 'unknown';
                        iframeInformationObject["platform"] = iframeContentNavigator["platform"] || 'unknown';
                        iframeInformationObject["do_not_track"] = iframeContentNavigator["doNotTrack"] || 'unknown';

                        performanceTimer["startInternal"]("plugins");
                        var isInternetExplorer = false;
                        if (iframeContentNavigator["appName"] === "Microsoft Internet Explorer") {
                            isInternetExplorer = true;
                        } else if (iframeContentNavigator["appName"] === "Netscape" && tridentRegex["test"](iframeContentNavigator["userAgent"])) {
                            isInternetExplorer = true;
                        }

                        var validActiveXAppNames = [];
                        if (iframeContentWindow["ActiveXObject"]) {
                            const ACTIVE_X_APP_NAMES = ["AcroPDF.PDF", "Adodb.Stream", "AgControl.AgControl", "DevalVRXCtrl.DevalVRXCtrl.1", "MacromediaFlashPaper.MacromediaFlashPaper", "Msxml2.DOMDocument", "Msxml2.XMLHTTP", "PDF.PdfCtrl", "QuickTime.QuickTime", "QuickTimeCheckObject.QuickTimeCheck.1", "RealPlayer", "RealPlayer.RealPlayer(tm) ActiveX Control (32-bit)", "RealVideo.RealVideo(tm) ActiveX Control (32-bit)", "Scripting.Dictionary", "SWCtl.SWCtl", "Shell.UIHelper", "ShockwaveFlash.ShockwaveFlash", "Skype.Detection", "TDCCtl.TDCCtl", "WMPlayer.OCX", "rmocx.RealPlayer G2 Control", "rmocx.RealPlayer G2 Control.1"];
                            let activeXTest = (activeXAppName) => {
                                try {
                                    new window["ActiveXObject"](activeXAppName);
                                    return activeXAppName;
                                } catch (exception) {
                                    return null;
                                }
                            }
                            for (let i = 0; i < ACTIVE_X_APP_NAMES.length; i++) {
                                let objectName = ACTIVE_X_APP_NAMES[i];
                                if (!ACTIVE_X_APP_NAMES.hasOwnProperty(i)) {
                                    continue;
                                }

                                validActiveXAppNames.push(activeXTest(objectName));
                            }
                        }

                        var sortedPluginList = [];
                        for (let i = 0; i < iframeContentNavigator["plugins"]["length"]; i++) {
                            sortedPluginList["push"](iframeContentNavigator["plugins"][i]);
                        }
                        sortedPluginList["sort"](function(firstElement, secondElement) {
                            if (firstElement["name"] > secondElement["name"]) {
                                return 1;
                            } else if (firstElement["name"] < secondElement["name"]) {
                                return -1;
                            } else {
                                return 0;
                            }
                        });

                        var pluginStrings = [];
                        let pluginStringGenerator = (pluginObject) => {
                            let pluginProperties = [];
                            for (let pluginProperty in pluginObject) {
                                let pluginPropertyValue = pluginObject[pluginProperty];

                                if (!plugin.hasOwnProperty(pluginProperty)) {
                                    continue;
                                }

                                let pluginPropertyString = [pluginPropertyValue["type"], pluginPropertyValue["suffixes"]]["join"]("~");
                                pluginProperties["push"](pluginPropertyString);
                            }
                            return [plugin["name"], plugin["description"], pluginProperties]["join"]("::");
                        }

                        for (let i = 0; i < sortedPluginList.length; i++) {
                            let pluginListItem = sortedPluginList[i];
                            if (!sortedPluginList.hasOwnProperty(pluginListItem)) {
                                continue;
                            }

                            pluginStrings["push"](pluginStringGenerator(pluginListItem));
                        }

                        var pluginConcatString = pluginStrings["join"](";");
                        performanceTimer["stopInternal"]("plugins");

                        iframeInformationObject["plugins"] = isInternetExplorer ? validActiveXAppNames["join"](";") : pluginConcatString;
                        var pluginsMetaObject = {};
                        try {
                            pluginsMetaObject["named_item_name"] = window["navigator"]["plugins"]["namedItem"]["name"];
                            pluginsMetaObject["item_name"] = window["navigator"]["plugins"]["item"]["name"];
                            pluginsMetaObject["refresh_name"] = window["navigator"]["plugins"]["refresh"]["name"];
                        } catch (exception) {}

                        iframeInformationObject["plugins_meta"] = pluginsMetaObject;

                        performanceTimer["startInternal"]("canvas_d");
                        var canvasElement = documentReference["createElement"]("canvas");
                        canvasElement["width"] = 600;
                        canvasElement["height"] = 160;
                        canvasElement["style"]["display"] = "inline";
                        var canvas2dContext = canvasElement["getContext"]("2d");
                        canvas2dContext["rect"](1, 1, 11, 11);
                        canvas2dContext["rect"](3, 3, 7, 7);
                        canvasInfoObject["winding"] = canvas2dContext["isPointInPath"](6, 6, "evenodd") === false;
                        try {
                            var secondCanvasElement = documentReference["createElement"]("canvas");
                            secondCanvasElement["width"] = 1;
                            secondCanvasElement["height"] = 1;
                            var canvasDataURL = secondCanvasElement["toDataURL"]("image/webp");
                            canvasInfoObject["towebp"] = (0 === canvasDataURL["indexOf"]("data:image/webp"));
                        } catch (canvasDataURLException) {
                            canvasInfoObject["towepb"] = "error";
                        }
                        canvasInfoObject["blending"] = (function() {
                            var isGlobalCompositeOperationScreen = false;
                            try {
                                var blendingCanvas = documentReference["createElement"]("canvas");
                                var blending2dContext = blendingCanvas["getContext"]("2d");
                                blending2dContext["globalCompositeOperation"] = "screen";
                                isGlobalCompositeOperationScreen = "screen" === blending2dContext["globalCompositeOperation"];
                            } catch (blendingException) {}
                            return isGlobalCompositeOperationScreen;
                        })();
                        canvas2dContext["textBaseline"] = "alphabetic";
                        canvas2dContext["fillStyle"] = "#f60";
                        canvas2dContext["fillRect"](125, 1, 62, 20);
                        canvas2dContext["fillStyle"] = "#069";
                        canvas2dContext["font"] = "11pt Arial";
                        canvas2dContext["fillText"]("Cwm fjordbank glyphs vext quiz,", 2, 15);
                        canvas2dContext["fillStyle"] = "rgba(102, 204, 0, 0.7)";
                        canvas2dContext["font"] = "18pt Arial";
                        canvas2dContext["fillText"]("Cwm fjordbank glyphs vext quiz,", 4, 45);
                        try {
                            canvas2dContext["globalCompositeOperation"] = "multiply";
                        } catch (globalCompositeMultiplyException) {}
                        canvas2dContext["fillStyle"] = "rgb(255,0,255)";
                        canvas2dContext["beginPath"]();
                        canvas2dContext["arc"](50, 50, 50, 0, 2 * window["Math"]["PI"], true);
                        canvas2dContext["closePath"]();
                        canvas2dContext["fill"]();
                        canvas2dContext["fillStyle"] = "rgb(0,255,255)";
                        canvas2dContext["beginPath"]();
                        canvas2dContext["arc"](100, 50, 50, 0, 2 * window["Math"]["PI"], true);
                        canvas2dContext["closePath"]();
                        canvas2dContext["fill"]();
                        canvas2dContext["fillStyle"] = "rgb(255,255,0)";
                        canvas2dContext["beginPath"]();
                        canvas2dContext["arc"](75, 100, 50, 0, 2 * window["Math"]["PI"], true);
                        canvas2dContext["closePath"]();
                        canvas2dContext["fill"]();
                        canvas2dContext["fillStyle"] = "rgb(255,0,255)";
                        canvas2dContext["arc"](75, 75, 75, 0, 2 * window["Math"]["PI"], true);
                        canvas2dContext["arc"](75, 75, 25, 0, 2 * window["Math"]["PI"], true);
                        canvas2dContext["fill"]("evenodd");
                        canvasDataURL = canvasElement["toDataURL"]();
                        performanceTimer["stopInternal"]("canvas_d");
                    });
                    fingerprintingFunctions["push"](function() {
                        performanceTimer["startInternal"]("canvas_h");
                        canvasDataHash = Mj(canvasDataURL); //I _think_ this is a hash of the canvasDataURL, but we'll need to fix some control flow to be sure
                        performanceTimer["stopInternal"]("canvas_h");
                        performanceTimer["startInternal"]("canvas_o");
                        var randomNumberGenerator_2 = randomNumberGeneratorFactory(2284030616, randomInteger);
                        var randomMax255Numbers_2 = [];
                        for (let i = 0; i < 3; i++ ) {
                            randomMax255Numbers_2.push(randomNumberGenerator_2() & 255);
                        }

                        performanceTimer["startInternal"]("canvas_io");
                        var randomNumberGenerator_3 = randomNumberGeneratorFactory(638959349, randomInteger);
                        var randomMax255Numbers_3 = [];
                        for (let i = 0; i < 31; i++) {
                            randomMax255Numbers_3.push(randomNumberGenerator_3() & 255);
                        }

                        function canvasDataInfoStringFix(infoString, randomNumbersArray, randomNumberIndexCeiling) {
                            let canvasDataInfoString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);
                            let characterArray = canvasDataInfoString.split("");
                            let delimitedCharacterArray = [];
                            for (let i = 0; i + 1 < characterArray.length; i += 2) {
                                delimitedCharacterArray.push(characterArray[i + 1]);

                                let randomCharacter = window.String.fromCharCode(randomNumbersArray[i % randomNumberIndexCeiling]);
                                delimitedCharacterArray.push(randomCharacter);

                                delimitedCharacterArray.push(characterArray[i]);

                                randomCharacter = window.String.fromCharCode(randomNumbersArray[(i + 1) % randomNumberIndexCeiling]);
                                delimitedCharacterArray.push(randomCharacter);
                              }
                            return window.btoa(delimitedCharacterArray.join(""));

                            //Remove unsafe unicode
                            //Convert string to char array
                            //Switch every 2nd character with the character before: [0, 1, 2, 3] becomes: [1, 0, 3, 2]
                            //Insert a random character (based on the random numbers array) after every character)
                            //Join the character array together to make a string
                            //Convert to base64
                            //return the base64 string
                        }

                        var canvasDataString = window.JSON.stringify(canvasDataHash, jsonStringUndefinedReplace);
                        var canvasDataStringDelimitedBase64 = canvasDataInfoStringFix(canvasDataString, randomMax255Numbers_3, 30);
                        canvasInfoObject["img"] = canvasDataStringDelimitedBase64;
                        performanceTimer["stopInternal"]("canvas_io");

                        function canvasInfoStringFix(infoString, randomNumbersArray) {
                            let safeString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);
                            let characterArray = safeString.split("");
                            let shiftedArray = new Array(characterArray.length);

                            shiftedArray = performDoubleShift(characterArray, randomNumbersArray[0], randomNumbersArray[1]);
                            let swappedArray = swapEverySecondElement(shiftedArray);
                            return window.btoa(
                                swappedArray.reduce(
                                    (result, currentValue) => result += currentValue,
                                    ""
                                )
                            );
                        }

                        var canvasInfoJsonString = window.JSON.stringify(canvasInfoObject, jsonStringUndefinedReplace);
                        iframeInformationObject["canvas"] = canvasInfoStringFix(canvasInfoJsonString, randomMax255Numbers_2);
                        performanceTimer["stopInternal"]("canvas_o");
                    });
                    fingerprintingFunctions["push"](function() {
                        performanceTimer["startInternal"]("webgl_cc");
                        var webGlCanvas = documentReference["createElement"]("canvas");
                        try {
                            webGlContext = webGlCanvas["getContext"]("webgl") || webGlCanvas["getContext"]("experimental-webgl");
                        } catch (webGlException) {}
                        performanceTimer["stopInternal"]("webgl_cc");
                    });
                    fingerprintingFunctions["push"](function() {
                        performanceTimer["startInternal"]("webgl_d");
                        var innerWebGlContext = webGlContext;
                        if (innerWebGlContext) {
                            var getFirstTwoArrayElements = function(someArray) {
                                return someArray ? [someArray[0], someArray[1]] : null;
                            };
                            var getMaxAnisotropy = function(webGlObject) {
                                var anistropyValue = null;
                                var anistropyExtension = webGlObject["getExtension"]("EXT_texture_filter_anisotropic") || webGlObject["getExtension"]("WEBKIT_EXT_texture_filter_anisotropic") || webGlObject["getExtension"]("MOZ_EXT_texture_filter_anisotropic'");
                                if (anistropyExtension) {
                                    var Vm = webGlObject["getParameter"](anistropyExtension["MAX_TEXTURE_MAX_ANISOTROPY_EXT"]);
                                    anistropyValue = Vm === 0 ? 2 : Vm;
                                }
                                return anistropyValue;
                            };
                            
                            var vertexShaderTemplate = "attribute vec2 attrVertex;varying vec2 varyinTexCoordinate;uniform vec2 uniformOffset;void main(){varyinTexCoordinate=attrVertex+uniformOffset;gl_Position=vec4(attrVertex,0,1);}";
                            var fragmentShaderTemplate = "precision mediump float;varying vec2 varyinTexCoordinate;void main() {gl_FragColor=vec4(varyinTexCoordinate,0,1);}";
                            var vertexPosBuffer = null;
                            if (innerWebGlContext["createBuffer"]) {
                                vertexPosBuffer = innerWebGlContext["createBuffer"]();
                            }
                            if (vertexPosBuffer) {
                                innerWebGlContext["bindBuffer"](innerWebGlContext["ARRAY_BUFFER"], vertexPosBuffer);
                                var vertices = new window["Float32Array"]([-0.2, -0.9, 0, 0.4, -0.26, 0, 0, 0.732134444, 0]);
                                innerWebGlContext["bufferData"](innerWebGlContext["ARRAY_BUFFER"], vertices, innerWebGlContext["STATIC_DRAW"]);
                                vertexPosBuffer["itemSize"] = 3;
                                vertexPosBuffer["numItems"] = 3;
                                var webGlProgram = innerWebGlContext["createProgram"]();
                                var vertexShader = innerWebGlContext["createShader"](innerWebGlContext["VERTEX_SHADER"]);
                                innerWebGlContext["shaderSource"](vertexShader, vertexShaderTemplate);
                                innerWebGlContext["compileShader"](vertexShader);
                                var fragmentShader = innerWebGlContext["createShader"](innerWebGlContext["FRAGMENT_SHADER"]);
                                innerWebGlContext["shaderSource"](fragmentShader, fragmentShaderTemplate);
                                innerWebGlContext["compileShader"](fragmentShader);
                                innerWebGlContext["attachShader"](webGlProgram, vertexShader);
                                innerWebGlContext["attachShader"](webGlProgram, fragmentShader);
                                innerWebGlContext["linkProgram"](webGlProgram);
                                innerWebGlContext["useProgram"](webGlProgram);
                                webGlProgram["vertexPosAttrib"] = innerWebGlContext["getAttribLocation"](webGlProgram, "attrVertex");
                                if (webGlProgram["vertexPosAttrib"] === -1) {
                                    webGlProgram["vertexPosAttrib"] = 0;
                                }
                                webGlProgram["offsetUniform"] = innerWebGlContext["getUniformLocation"](webGlProgram, "uniformOffset");
                                if (webGlProgram["offsetUniform"] === -1) {
                                    webGlProgram["offsetUniform"] = 0;
                                }
                                innerWebGlContext["enableVertexAttribArray"](webGlProgram["vertexPosArray"]);
                                innerWebGlContext["vertexAttribPointer"](webGlProgram["vertexPosAttrib"], vertexPosBuffer["itemSize"], innerWebGlContext["FLOAT"], false, 0, 0);
                                innerWebGlContext["uniform2f"](webGlProgram["offsetUniform"], 1, 1);
                                innerWebGlContext["drawArrays"](innerWebGlContext["TRIANGLE_STRIP"], 0, vertexPosBuffer["numItems"]);
                                if (innerWebGlContext["canvas"] !== null) {
                                    webGlInfoObject["img"] = null;
                                    webGlCanvasDataURL = innerWebGlContext["canvas"]["toDataURL"]();
                                }
                            }
                            var webGlExtensions = innerWebGlContext["getSupportedExtensions"] && innerWebGlContext["getSupportedExtensions"]();
                            webGlInfoObject["extensions"] = webGlExtensions ? webGlExtensions["join"](";") : null;
                            webGlInfoObject["aliased_line_width_range"] = getFirstTwoArrayElements(innerWebGlContext["getParameter"](innerWebGlContext["ALIASED_LINE_WIDTH_RANGE"]));
                            webGlInfoObject["aliased_point_size_range"] = getFirstTwoArrayElements(innerWebGlContext["getParameter"](innerWebGlContext["ALIASED_POINT_SIZE_RANGE"]));
                            webGlInfoObject["alpha_bits"] = innerWebGlContext["getParameter"](innerWebGlContext["ALPHA_BITS"]);

                            var webGlContextAttributes = null;
                            if (!!innerWebGlContext["getContextAttributes"]) {
                                webGlContextAttributes = innerWebGlContext["getContextAttributes"]();
                            }

                            if (!!!webGlContextAttributes) {
                                webGlInfoObject["antialiasing"] = null;
                            } else if (webGlContextAttributes["antialias"]) {
                                webGlInfoObject["antialiasing"] = true;
                            } else {
                                webGlInfoObject["antialiasing"] = false;
                            }

                            webGlInfoObject["blue_bits"] = innerWebGlContext["getParameter"](innerWebGlContext["BLUE_BITS"]);
                            webGlInfoObject["depth_bits"] = innerWebGlContext["getParameter"](innerWebGlContext["DEPTH_BITS"]);
                            webGlInfoObject["green_bits"] = innerWebGlContext["getParameter"](innerWebGlContext["GREEN_BITS"]);
                            webGlInfoObject["max_anisotropy"] = getMaxAnisotropy(innerWebGlContext);
                            webGlInfoObject["max_combined_texture_image_units"] = innerWebGlContext["getParameter"](innerWebGlContext["MAX_COMBINED_TEXTURE_IMAGE_UNITS"]);
                            webGlInfoObject["max_cube_map_texture_size"] = innerWebGlContext["getParameter"](innerWebGlContext["MAX_CUBE_MAP_TEXTURE_SIZE"]);
                            webGlInfoObject["max_fragment_uniform_vectors"] = innerWebGlContext["getParameter"](innerWebGlContext["MAX_FRAGMENT_UNIFORM_VECTORS"]);
                            webGlInfoObject["max_render_buffer_size"] = innerWebGlContext["getParameter"](innerWebGlContext["MAX_RENDERBUFFER_SIZE"]);
                            webGlInfoObject["max_texture_image_units"] = innerWebGlContext["getParameter"](innerWebGlContext["MAX_TEXTURE_IMAGE_UNITS"]);
                            webGlInfoObject["max_texture_size"] = innerWebGlContext["getParameter"](innerWebGlContext["MAX_TEXTURE_SIZE"]);
                            webGlInfoObject["max_varying_vectors"] = innerWebGlContext["getParameter"](innerWebGlContext["MAX_VARYING_VECTORS"]);
                            webGlInfoObject["max_vertex_attribs"] = innerWebGlContext["getParameter"](innerWebGlContext["MAX_VERTEX_ATTRIBS"]);
                            webGlInfoObject["max_vertex_texture_image_units"] = innerWebGlContext["getParameter"](innerWebGlContext["MAX_VERTEX_TEXTURE_IMAGE_UNITS"]);
                            webGlInfoObject["max_vertex_uniform_vectors"] = innerWebGlContext["getParameter"](innerWebGlContext["MAX_VERTEX_UNIFORM_VECTORS"]);
                            webGlInfoObject["max_viewport_dims"] = getFirstTwoArrayElements(innerWebGlContext["getParameter"](innerWebGlContext["MAX_VIEWPORT_DIMS"]));
                            webGlInfoObject["red_bits"] = innerWebGlContext["getParameter"](innerWebGlContext["RED_BITS"]);
                            webGlInfoObject["renderer"] = innerWebGlContext["getParameter"](innerWebGlContext["RENDERER"]);
                            webGlInfoObject["shading_language_version"] = innerWebGlContext["getParameter"](innerWebGlContext["SHADING_LANGUAGE_VERSION"]);
                            webGlInfoObject["stencil_bits"] = innerWebGlContext["getParameter"](innerWebGlContext["STENCIL_BITS"]);
                            webGlInfoObject["vendor"] = innerWebGlContext["getParameter"](innerWebGlContext["VENDOR"]);
                            webGlInfoObject["version"] = innerWebGlContext["getParameter"](innerWebGlContext["VERSION"]);
                            if (innerWebGlContext["getShaderPrecisionFormat"]) {
                                var shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["VERTEX_SHADER"], innerWebGlContext["HIGH_FLOAT"]);
                                if (shaderPrecisionFormat) {
                                    webGlInfoObject["vertex_shader_high_float_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["vertex_shader_high_float_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["vertex_shader_high_float_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["VERTEX_SHADER"], innerWebGlContext["MEDIUM_FLOAT"]);
                                    webGlInfoObject["vertex_shader_medium_float_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["vertex_shader_medium_float_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["vertex_shader_medium_float_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["VERTEX_SHADER"], innerWebGlContext["LOW_FLOAT"]);
                                    webGlInfoObject["vertex_shader_low_float_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["vertex_shader_low_float_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["vertex_shader_low_float_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["FRAGMENT_SHADER"], innerWebGlContext["HIGH_FLOAT"]);
                                    webGlInfoObject["fragment_shader_high_float_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["fragment_shader_high_float_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["fragment_shader_high_float_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["FRAGMENT_SHADER"], innerWebGlContext["MEDIUM_FLOAT"]);
                                    webGlInfoObject["fragment_shader_medium_float_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["fragment_shader_medium_float_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["fragment_shader_medium_float_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["FRAGMENT_SHADER"], innerWebGlContext["LOW_FLOAT"]);
                                    webGlInfoObject["fragment_shader_low_float_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["fragment_shader_low_float_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["fragment_shader_low_float_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["VERTEX_SHADER"], innerWebGlContext["HIGH_INT"]);
                                    webGlInfoObject["vertex_shader_high_int_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["vertex_shader_high_int_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["vertex_shader_high_int_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["VERTEX_SHADER"], innerWebGlContext["MEDIUM_INT"]);
                                    webGlInfoObject["vertex_shader_medium_int_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["vertex_shader_medium_int_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["vertex_shader_medium_int_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["VERTEX_SHADER"], innerWebGlContext["LOW_INT"]);
                                    webGlInfoObject["vertex_shader_low_int_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["vertex_shader_low_int_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["vertex_shader_low_int_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["FRAGMENT_SHADER"], innerWebGlContext["HIGH_INT"]);
                                    webGlInfoObject["fragment_shader_high_int_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["fragment_shader_high_int_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["fragment_shader_high_int_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["FRAGMENT_SHADER"], innerWebGlContext["MEDIUM_INT"]);
                                    webGlInfoObject["fragment_shader_medium_int_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["fragment_shader_medium_int_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["fragment_shader_medium_int_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                    shaderPrecisionFormat = innerWebGlContext["getShaderPrecisionFormat"](innerWebGlContext["FRAGMENT_SHADER"], innerWebGlContext["LOW_INT"]);
                                    webGlInfoObject["fragment_shader_low_int_precision"] = shaderPrecisionFormat["precision"];
                                    webGlInfoObject["fragment_shader_low_int_precision_range_min"] = shaderPrecisionFormat["rangeMin"];
                                    webGlInfoObject["fragment_shader_low_int_precision_range_max"] = shaderPrecisionFormat["rangeMax"];
                                }
                            }
                            var debugRenderInfo = innerWebGlContext["getExtension"]("WEBGL_debug_renderer_info");
                            if (debugRenderInfo) {
                                if (innerWebGlContext["getParameter"](debugRenderInfo["UNMASKED_VENDOR_WEBGL"]) !== undefined) {
                                    webGlInfoObject["unmasked_vendor"] = innerWebGlContext["getParameter"](debugRenderInfo["UNMASKED_VENDOR_WEBGL"]);
                                }
                                if (innerWebGlContext["getParameter"](debugRenderInfo["UNMASKED_RENDERER_WEBGL"]) !== undefined) {
                                    webGlInfoObject["unmasked_renderer"] = innerWebGlContext["getParameter"](debugRenderInfo["UNMASKED_RENDERER_WEBGL"]);
                                }
                            }
                        }
                        performanceTimer["stopInternal"]("webgl_d");
                    });
                    fingerprintingFunctions["push"](function() {
                        performanceTimer["startInternal"]("webgl_h");
                        if (webGlCanvasDataURL) {
                            webGlCanvasDataHash = Mj(webGlCanvasDataURL);
                        }
                        performanceTimer["stopInternal"]("webgl_h");
                    });
                    fingerprintingFunctions["push"](function() {
                        performanceTimer["startInternal"]("webgl_o");
                        var randomNumberGenerator_4 = randomNumberGeneratorFactory(430797680, randomInteger);
                        var random255MaxNumbers_3 = [];
                        var arrayIndex_1 = 0;
                        while (arrayIndex_1 < 22) {
                            random255MaxNumbers_3.push(randomNumberGenerator_4() & 255);
                            arrayIndex_1 += 1;
                        }
                        performanceTimer["startInternal"]("webgl_io");

                        if (webGlCanvasDataHash) {
                            var randomNumberGenerator_5 = randomNumberGeneratorFactory(4143207636, randomInteger);
                            var random255MaxNumbers_4 = [];
                            var arrayIndex_2 = 0;
                            while (arrayIndex_2 < 26) {
                                random255MaxNumbers_4.push(randomNumberGenerator_5() & 255);
                                arrayIndex_2 += 1;
                            }
                            var webGlCanvasDataString = window.JSON.stringify(webGlCanvasDataHash, jsonStringUndefinedReplace);
                            function webGlCanvasDataFix(canvasDataString, randomNumberArray, randomNumberLimit) {
                                let safeString = canvasDataString.replace(unsafeCharRegex, convertFirstCharToUnicode);
                                let characterArray = safeString.split("");
                                let modifiedString = characterArray.reduce((outputString, currentValue, currentIndex) => {
                                    let charCode = currentValue.charCodeAt();
                                    outputString += window.String.fromCharCode(charCode << 4 & 240 | charCode >> 4);
                                    outputString += window.String.fromCharCode(randomNumberArray[currentIndex % randomNumberLimit]);
                                    return outputString;
                                }, "");

                                return modifiedString;
                            }

                            var base64delimitedModifiedWebGlString = window.btoa(webGlCanvasDataFix(webGlCanvasDataString, random255MaxNumbers_4["slice"](0, 25), 25));

                            webGlInfoObject["img"] = base64delimitedModifiedWebGlString;
                        }
                        performanceTimer["stopInternal"]("webgl_io");
                        var webGlInfoObject = webGlInfoObject;
                        var webGlInfoJsonString = window.JSON.stringify(webGlInfoObject, jsonStringUndefinedReplace);


                        function webGlInfoFix(infoString, randomNumbers, randomNumberLimit, randomOffsets) {
                            let safeString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);
                            let characterArray = safeString.split("");
                            let randomCharacterArray = characterArray.map((currentValue, currentIndex) => {
                                let charCode = currentValue.charCodeAt();
                                let randomNumber = randomNumbers[currentIndex % randomNumberLimit];
                                return window.String.fromCharCode(charCode ^ randomNumber);
                            }, this);

                            let shiftedCharacterArray = performDoubleShift(randomCharacterArray, randomOffsets[0], randomOffsets[1]);
                            let swappedArray = swapEverySecondElement(shiftedCharacterArray);

                            return swappedArray.join("");
                        }

                        let fixedWebGlInfoString = webGlInfoFix(webGlInfoJsonString, random255MaxNumbers_3["slice"](0, 19), 19, [random255MaxNumbers_3[19], random255MaxNumbers_3[20]]);
                        iframeInformationObject["web_gl"] = window.btoa(fixedWebGlInfoString);
                        performanceTimer["stopInternal"]("webgl_o");
                    });
                    fingerprintingFunctions["push"](function() {
                        performanceTimer["startInternal"]("webgl_meta");
                        var webGlMetaData = {};
                        try {
                            webGlMetaData["get_parameter_name"] = window["WebGLRenderingContext"]["prototype"]["getParameter"]["name"];
                            webGlMetaData["get_parameter_native"] = isFunctionNative(window["WebGLRenderingContext"]["prototype"]["getParameter"]);
                        } catch (metaException) {}
                        performanceTimer["stopInternal"]("webgl_meta");

                        iframeInformationObject["web_gl_meta"] = webGlMetaData;
                        var randomNumberGenerator_6 = randomNumberGeneratorFactory(764395007, randomInteger);
                        var random255MaxNumbers_5 = [];
                        for (let i = 0; i < 2; i++) {
                            random255MaxNumbers_5.push(randomNumberGenerator_6() & 255);
                        }
                        var touchDataObject = {};
                        if (typeof(iframeContentNavigator["maxTouchPoints"]) !== "undefined") {
                            touchDataObject["max_touch_points"] = iframeContentNavigator["maxTouchPoints"];
                        } else if (typeof(iframeContentNavigator["msMaxTouchPoints"]) !== "undefined") {
                            touchDataObject["max_touch_points"] = iframeContentNavigator["msMaxTouchPoints"];
                        } else {
                            touchDataObject["max_touch_points"] = 0;
                        }
                        try {
                            documentReference["createEvent"]("TouchEvent");
                            touchDataObject["touch_event"] = true;
                        } catch (touchException) {
                            touchDataObject["touch_event"] = false;
                        }
                        touchDataObject["touch_start"] = iframeContentWindow["ontouchstart"] !== undefined;
                        var touchDataObject = touchDataObject;
                        var touchDataJsonString = window.JSON.stringify(touchDataObject, jsonStringUndefinedReplace);

                        function touchDataInfoFix(infoString, shiftOffset) {
                            let safeString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);
                            let touchDataArray = safeString.split("");
                            let swappedArray = swapEverySecondElement(touchDataArray);
                            let shiftedArray = performSingleArrayShift(swappedArray, shiftOffset);
                            return shiftedArray.join("");
                        }

                        let fixedTouchDataString = touchDataInfoFix(touchDataJsonString, random255MaxNumbers_5[0]);
                        iframeInformationObject["touch"] = window.btoa(fixedTouchDataString);

                        var randomNumberGenerator_7 = randomNumberGeneratorFactory(2514653307, randomInteger);
                        var randomMax255Numbers_6 = [];
                        for(let i = 0; i < 37; i++ ) {
                            randomMax255Numbers_6.push(randomNumberGenerator_7() & 255);
                        }
                        performanceTimer["startInternal"]("video");
                        var iframeVideoElement = iframeContentDocument["createElement"]("video");
                        var supportedVideoTypeData;
                        try {
                            if (!!iframeVideoElement["canPlayType"]) {
                                supportedVideoTypeData = {};
                                supportedVideoTypeData["ogg"] = iframeVideoElement["canPlayType"]("video/ogg; codecs=\"theora\"") || "nope";
                                supportedVideoTypeData["h264"] = iframeVideoElement["canPlayType"]("video/mp4; codecs=\"avc1.42E01E\"") || "nope";
                                supportedVideoTypeData["webm"] = iframeVideoElement["canPlayType"]("video/webm; codecs=\"vp8, vorbis\"") || "nope";
                            }
                        } catch (videoException) {
                            supportedVideoTypeData = "errored";
                        }
                        performanceTimer["stopInternal"]("video");
                        var supportedVideoTypeData = supportedVideoTypeData;
                        var supportedVideoTypeString = window.JSON.stringify(supportedVideoTypeData, jsonStringUndefinedReplace);

                        function videoStringFix(infoString, randomNumberArray1, randomNumberArray2) {
                            let safeString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);

                            return safeString.split("").map((character, currentIndex) =>
                                window.String.fromCharCode((character.charCodeAt() ^ randomNumberArray1[currentIndex % 18]) ^ randomNumberArray2[currentIndex % 18])
                            ).reverse().join("");

                            //convert to char codes
                            //Xor with a random number
                            //Xor with a random number (again)
                            //reverse the array
                            //Concat as a string
                        }

                        iframeInformationObject["video"] = window.btoa(
                            videoStringFix(
                                supportedVideoTypeString, randomMax255Numbers_6.slice(0, 18), randomMax255Numbers_6.slice(18, 36)
                            )
                        );

                        var randomNumberGenerator_8 = randomNumberGeneratorFactory(836013910, randomInteger);
                        var randomMax255Numbers_6 = [];
                        for (let i = 0; i < 3; i++) {
                            randomMax255Numbers_6.push(randomNumberGenerator_8() & 255);
                        }

                        performanceTimer["startInternal"]("audio");
                        var audioElement = iframeContentDocument["createElement"]("audio");
                        var audioInfoObject = false;
                        if (!!audioElement["canPlayType"]) {
                            audioInfoObject = {};
                            audioInfoObject["ogg"] = audioElement["canPlayType"]("audio/ogg; codecs=\"vorbis\"") || "nope";
                            audioInfoObject["mp3"] = audioElement["canPlayType"]("audio/mpeg") || "nope";
                            audioInfoObject["wav"] = audioElement["canPlayType"]("audio/wav; codecs=\"1\"") || "nope";
                            audioInfoObject["m4a"] = audioElement["canPlayType"]("audio/x-m4a;") || audioElement["canPlayType"]("audio/aac;") || "nope";
                        }
                        performanceTimer["stopInternal"]("audio");
                        var audioInfoObject = audioInfoObject;
                        var audioInfoString = window.JSON.stringify(audioInfoObject, jsonStringUndefinedReplace);

                        function audioInfoFix(infoString, randomNumberOffset1, randomNumberOffset2) {
                            let safeString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);
                            let charArray = safeString.split("");
                            let swappedArray = swapEverySecondElement(charArray);
                            let shiftedArray = performDoubleShift(swappedArray, randomNumberOffset1, randomNumberOffset2);
                            return shiftedArray.join("");
                        }

                        iframeInformationObject["audio"] = window.btoa(audioInfoFix(audioInfoString, randomMax255Numbers_6[0], randomMax255Numbers_6[1]));
                        iframeInformationObject["vendor"] = iframeContentNavigator["vendor"];
                        iframeInformationObject["product"] = iframeContentNavigator["product"];
                        iframeInformationObject["product_sub"] = iframeContentNavigator["productSub"];
                        
                        var randomNumberGenerator_9 = randomNumberGeneratorFactory(694216168, randomInteger);
                        var randomMax255Numbers_7 = [];
                        for (let i = 0; i < 3; i++) {
                            randomMax255Numbers_7.push(randomNumberGenerator_9() & 255);
                        }

                        var browserInfoObject = {};
                        var chromeElement = iframeContentWindow["chrome"];
                        var chromeExists = chromeElement !== null && typeof(chromeElement) === "object";
                        var isInternetExplorer = iframeContentNavigator["appName"] === "Microsoft Internet Explorer" || iframeContentNavigator["appName"] === "Netscape" && tridentRegex["test"](iframeContentNavigator["userAgent"]);
                        browserInfoObject["ie"] = isInternetExplorer;
                        if (chromeExists) {
                            try {
                                var loadTimesObject = {};
                                loadTimesObject["load_times_native"] = isFunctionNative(iframeContentWindow["chrome"]["loadTimes"]);
                                browserInfoObject["chrome"] = loadTimesObject;
                            } catch (ignoredException) {}
                        }

                        browserInfoObject["webdriver"] = iframeContentNavigator["webdriver"] ? true : false;
                        var browserInfoString = window.JSON.stringify(browserInfoObject, jsonStringUndefinedReplace);

                        function browserInfoFix(infoString, shift1, shift2) {
                            let safeString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);
                            return performDoubleShift(safeString.split(""), shift1, shift2).join("");
                        }

                        iframeInformationObject["browser"] = window.btoa(browserInfoFix(browserInfoString, randomMax255Numbers_7[0], randomMax255Numbers_7[1]));
                        var randomNumberGenerator_10 = randomNumberGeneratorFactory(1513031664, randomInteger);
                        var randomMax255Numbers_8 = [];
                        for (let i = 0; i < 22; i++) {
                            randomMax255Numbers_8.push(randomNumberGenerator_10() & 255);
                        }

                        var moreBrowserInfoObject = {}; 
                        if (window["history"]["length"] !== undefined) {
                            moreBrowserInfoObject["history_length"] = window["history"]["length"];
                        }

                        if (window["navigator"]["hardwareConcurrency"] !== undefined) {
                            moreBrowserInfoObject["hardware_concurrency"] = window["navigator"]["hardwareConcurrency"];
                        }
                        moreBrowserInfoObject["iframe"] = window["self"] !== window["top"];
                        moreBrowserInfoObject["battery"] = isFunctionNative(window["navigator"]["getBattery"]);

                        try {
                            moreBrowserInfoObject["console_debug_name"] = window["console"]["debug"]["name"];
                        } catch (ignored) {}

                        try {
                            moreBrowserInfoObject["console_debug_native"] = isFunctionNative(window["console"]["debug"]);
                        } catch (ignored) {}
                        moreBrowserInfoObject["has_underscore_phantom"] = window["_phantom"] !== undefined;
                        moreBrowserInfoObject["has_call_phantom"] = window["callPhantom"] !== undefined;
                        moreBrowserInfoObject["non_native_functions"] = [];
                        var moreBrowserInfoString = window.JSON.stringify(moreBrowserInfoObject, jsonStringUndefinedReplace);

                        function moreBrowserInfoFix(infoString, randomNumberArray, shift1, shift2) {
                            let safeString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);
                            let xordArray = safeString.split("").map((currentValue, currentIndex) => {
                                return window.String.fromCharCode(currentValue.charCodeAt() ^ randomNumberArray[currentIndex % 19])
                            });

                            let reversedArray = xordArray.reverse();
                            let shiftedArray = performDoubleShift(reversedArray, shift1, shift2);
                            return shiftedArray.join("");
                            //Convert to char code array
                            //Xor with a random number
                            //Reverse
                            //Perform a double shift
                            //Back to a string
                        }

                        iframeInformationObject["window"] = window.btoa(moreBrowserInfoFix(moreBrowserInfoString, randomMax255Numbers_8, randomMax255Numbers_8[19], randomMax255Numbers_8[20]));

                        var documentReferenceProtocol = {};
                        if (documentReference["location"]["protocol"] !== undefined) {
                            documentReferenceProtocol["protocol"] = documentReference["location"]["protocol"];
                        }
                        iframeInformationObject["location"] = documentReferenceProtocol;
                        performanceTimer["startInternal"]("canvas_fonts");
                        var baseFonts = ["monospace", "sans-serif", "serif"];
                        var fontList = ["ARNOPRO", "AgencyFB", "ArabicTypesetting", "ArialUnicodeMS", "AvantGardeBkBT", "BankGothicMdBT", "Batang", "BitstreamVeraSansMono", "Calibri", "Century", "CenturyGothic", "Clarendon", "EUROSTILE", "FranklinGothic", "FuturaBkBT", "FuturaMdBT", "GOTHAM", "GillSans", "HELV", "Haettenschweiler", "HelveticaNeue", "Humanst521BT", "Leelawadee", "LetterGothic", "LevenimMT", "LucidaBright", "LucidaSans", "MSMincho", "MSOutlook", "MSReferenceSpecialty", "MSUIGothic", "MTExtra", "MYRIADPRO", "Marlett", "MeiryoUI", "MicrosoftUighur", "MinionPro", "MonotypeCorsiva", "PMingLiU", "Pristina", "SCRIPTINA", "SegoeUILight", "Serifa", "SimHei", "SmallFonts", "Staccato222BT", "TRAJANPRO", "UniversCE55Medium", "Vrinda", "ZWAdobeF"];
                        var testString = "mmmmmmmmlli";
                        var testSize = "72px";
                        var floatEqualityRange = 0.1;
                        var areFloatsEqual = function(float1, float2) {
                            return float1 === float2 || window["Math"]["abs"](float1 - float2) < floatEqualityRange;
                        };
                        var canvas2DContext = documentReference["createElement"]("canvas")["getContext"]("2d");
                        var fontStringMeasurements = [];
                        for (var baseFont of baseFonts) {
                            canvas2DContext["font"] = testSize + " " + baseFont;
                            fontStringMeasurements["push"]([baseFont, canvas2DContext["measureText"](testString)]);
                        }
                        var supportedFonts = [];
                        //Reference which assisting in refactoring this loop: https://privacycheck.sec.lrz.de/active/fp_cf/fp_canvas_font.html
                        for (var font of fontList) {
                            var isFontSupported = false;
                            for (var fontStringMeasurement of fontStringMeasurements) {
                                if (!isFontSupported) {
                                    var fontName = fontStringMeasurement[0];
                                    var resultingMeasure = fontStringMeasurement[1];
                                    canvas2DContext["font"] = testSize + " " + font + ", " + fontName;
                                    var secondMeasure = canvas2DContext["measureText"](testString);
                                    try {
                                        if (!areFloatsEqual(secondMeasure["width"], resultingMeasure["width"]) ||
                                            !areFloatsEqual(secondMeasure["actualBoundingBoxAscent"], resultingMeasure["actualBoundingBoxAscent"]) ||
                                            !areFloatsEqual(secondMeasure["actualBoundingBoxDescent"], resultingMeasure["actualBoundingBoxDescent"]) ||
                                            !areFloatsEqual(secondMeasure["actualBoundingBoxLeft"], resultingMeasure["actualBoundingBoxLeft"]) ||
                                            !areFloatsEqual(secondMeasure["actualBoundingBoxRight"], resultingMeasure["actualBoundingBoxRight"])) {
                                            isFontSupported = true;
                                        }
                                    } catch (ignored) {}
                                }
                            }
                            if (isFontSupported) {
                                supportedFonts["push"](font);
                            }
                        }
                        performanceTimer["stopInternal"]("canvas_fonts");
                        iframeInformationObject["fonts_array"] = supportedFonts;
                        var documentInfoObject = {};
                        try {
                            var elementSourceList = [];
                            for (var childElement of window["document"]["documentElement"]["children"]) {
                                if (childElement["tagName"] === "SCRIPT" && elementSourceList["length"] < 10) {
                                    elementSourceList["push"]({"src": childElement["src"]});
                                }
                            }
                            documentInfoObject["document_element"] = elementSourceList;
                        } catch (exception) {}
                        try {
                            var headElementSources = [];
                            for (var headChildElement of window["document"]["head"]["children"]) {
                                if (headChildElement["tagName"] === "SCRIPT" && headElementSources["length"] < 10) {
                                    headElementSources["push"]({"src": headChildElement["src"]});
                                }
                            }
                            documentInfoObject["head"] = headElementSources;
                        } catch (exception) {}
                        iframeInformationObject["scripts"] = documentInfoObject;
                        var randomNumberGenerator_11 = randomNumberGeneratorFactory(187585459, randomInteger);
                        var randomMax255Numbers_9 = [];
                        for (let i = 0; i < 21; i++) {
                            randomMax255Numbers_9.push(randomNumberGenerator_11() & 255);
                        }

                        function checkErrorMessages() {
                            var functionApplyMessage = undefined;
                            try {
                                (function() {
                                    window["Function"]["prototype"]["toString"]["apply"](null);
                                })();
                            } catch (functionApplyException) {
                                if (functionApplyException !== undefined && functionApplyException !== null && functionApplyException["stack"] && functionApplyException["message"]) {
                                    functionApplyMessage = functionApplyException["message"];
                                }
                            }

                            var nullCallMessage = undefined;
                            try {
                                (function() {
                                    null["toString"]();
                                })();
                            } catch (nullCallException) {
                                if (nullCallException !== undefined && nullCallException !== null && nullCallException["stack"] && nullCallException["message"]) {
                                    nullCallMessage = nullCallException["message"];
                                }
                            }

                            return functionApplyMessage === nullCallMessage;
                        }

                        function checkWebglUnmasked() {
                            var UNMASKED_VENDOR_WEBGL = 37445;
                            var UNMASKED_RENDERER_WEBGL = 37446;
                            var hasUnmaskedVendor = true;
                            try {
                                window["WebGLRenderingContext"]["prototype"]["getParameter"]["call"](null, UNMASKED_VENDOR_WEBGL);
                            } catch (exception) {
                                hasUnmaskedVendor = false;
                            }
                            var hasUnmaskedRender = true;
                            try {
                                window["WebGLRenderingContext"]["prototype"]["getParameter"]["call"](null, UNMASKED_RENDERER_WEBGL);
                            } catch (exception) {
                                hasUnmaskedRender = false;
                            }
                            return hasUnmaskedVendor || hasUnmaskedRender;
                        }
                        var someInfoObject = {};
                        try {
                            someInfoObject["f0"] = checkErrorMessages();
                        } catch (exception) {}
                        try {
                            someInfoObject["f1"] = checkWebglUnmasked();
                        } catch (exception) {}

                        var someInfoString = window.JSON.stringify(someInfoObject, jsonStringUndefinedReplace);

                        function someInfoStringFix(infoString, randomNumberArray) {
                            let safeInfoString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);

                            let swappedArray = swapEverySecondElement(safeInfoString.split(""));
                            let delimitedCharArray = swappedArray.reduce((someVal, currentValue, currentIndex) => {
                                someVal.push(currentValue.charCodeAt());
                                someVal.push(randomNumberArray[currentIndex % 20])
                                return someVal;
                            }, []);

                            return delimitedCharArray.reduce((output, currentValue) => output += window.String.fromCharCode(currentValue), "");

                            //Char array
                            //Deep copy
                            //Swap every second
                            //Make delimited via random number array
                            //stringify
                        }
                        iframeInformationObject["environment"] = window.btoa(someInfoStringFix(someInfoString, randomMax255Numbers_9.slice(0, 20)));
                    });
                    fingerprintingFunctions["push"](function() {
                        var finalReturnObject = {};
                        performanceTimer["startInternal"]("prop_o");
                        var randomNumberGenerator_12 = randomNumberGeneratorFactory(1740574759, randomInteger);
                        var randomMax255Numbers_11 = [];
                        for (let i = 0; i < 51; i++) {
                            randomMax255Numbers_11.push(randomNumberGenerator_12() & 255);
                        }

                        var iframeInfoString = window.JSON.stringify(iframeInformationObject, jsonStringUndefinedReplace);

                        function iframeInfoStringFix(infoString, randomNumberArray, randomDelimiterArray) {
                            let safeString = infoString.replace(unsafeCharRegex, convertFirstCharToUnicode);
                            let swappedArray = swapEverySecondElement(safeString.split(""));
                            let xordArray = swappedArray.map((currentValue, currentIndex) => {
                                return currentValue.charCodeAt() ^ randomNumberArray[currentIndex % 20];
                            });

                            let delimitedCharArray = xordArray.reduce((someVal, currentValue, currentIndex) => {
                                someVal.push(currentValue);
                                someVal.push(randomDelimiterArray[currentIndex % 30])
                                return someVal;
                            }, []);

                            return delimitedCharArray.reduce((output, currentValue) => output += window.String.fromCharCode(currentValue), "");

                            //Convert to char array
                            //Swap every second element
                            //Xor with a random number
                            //Delimit with a random number
                            //Back to a string and rejoin
                        }
                        finalReturnObject["p"] = window.btoa(iframeInfoStringFix(iframeInfoString, randomMax255Numbers_11["slice"](0, 20), randomMax255Numbers_11["slice"](20, 50)));


                        performanceTimer["stopInternal"]("prop_o");
                        finalReturnObject["st"] = 1617321333;
                        finalReturnObject["sr"] = 118757315;
                        finalReturnObject["cr"] = randomInteger;
                        iframe["parentNode"]["baseRemoveChild_e421bb29"] = iframe["parentNode"]["__proto__"]["removeChild"];
                        iframe["parentNode"]["baseRemoveChild_e421bb29"](iframe);
                        performanceTimer["stop"]("interrogation");
                        onSuccessCallback(finalReturnObject);
                    });
                    var fingerprintFunctionIndex = 0;
                    var fingerprintFunctionRunner = function() {
                        var fingerprintFunction = fingerprintingFunctions[fingerprintFunctionIndex];
                        if (fingerprintFunction) {
                            try {
                                performanceTimer["startInternal"]("t" + fingerprintFunctionIndex);
                                fingerprintFunction();
                                performanceTimer["stopInternal"]("t" + fingerprintFunctionIndex);
                                fingerprintFunctionIndex += 1;
                                window["setTimeout"](fingerprintFunctionRunner, 0);
                            } catch (exception) {
                                exception["message"] = exception["message"] + " " + 1617321333 + " " + 118757315;
                                onErrorCallback(exception);
                            }
                        }
                    };

                    fingerprintFunctionRunner();
                } catch (exception) {
                    exception["message"] = exception["message"] + " " + 1617321333 + " " + 118757315;
                    onErrorCallback(exception);
                }
            });
            documentReference["body"]["insertBefore_e421bb29"] = documentReference["body"]["__proto__"]["insertBefore"];
            documentReference["body"]["insertBefore_e421bb29"](iframe, documentReference["body"]["firstChild"]);
        } catch (exception) {
            onErrorCallback(exception);
            exception["message"] = exception["message"] + " " + 1617321333 + " " + 118757315;
        }
    };
}

window["reese84interrogator"] = runBrowserInterrogation;
reese84(reeseCallParamArray);