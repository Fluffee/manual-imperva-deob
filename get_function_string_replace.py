#!/usr/bin/env python3

import sys
from typing import List, Set, Dict, Tuple, Optional
import re

function_strings = ["responseType", "replace", "content-type", "bingbot|msnbot|bingpreview|adsbot-google|googlebot|mediapartners-google|sogou|baiduspider|yandex.com/bots|yahoo.ad.monitoring|yahoo!.slurp", "defineProperty", "responseURL", "parse", "race", "blob", "Response", "pow", "require", "script", "lax", "none_secure", "shift", "indexOf", "trim", "hostname", "appendChild", "Post", "hasOwnProperty", "_subscribers", "Internet Explorer", "createElement", "_initBody", "old_token", "fromJson", " [ ", "credentials", "_start", "[object Uint16Array]", "documentElement", "TokenResponse", "OSX", "Blob", "Solution", "none_secure", "getTime", "Get", "umask", "Already read", "automationCheck", "application/x-www-form-urlencoded;charset=UTF-8", "measures", "INPUT", "A promises callback cannot return that same promise.", "include", "Request", "timerFactory", "trys", "label", "submitCaptcha timed out", "concat", "has", "_result", "response", "Timeout while retrieving token", "url", "isView", "CaptchaProvider", "substr", "PUT", "getAttribute", "MacIntel", "max", "postbackUrl", "method", "getToken", "legacy", "; max-age=", "': ", "Module", "runOnContext", "interrogatorFactory", "message", "reese84_", "$2$1", "userAgent", "POST", "x-d-test", "onTimeout", "fire", "getItem", "error", "search", "env", "reject", "", "[object Array]", "runAutomationCheck", "argv", "bon", "_enumerate", "charCodeAt", "withCredentials", "FormData", "cookieDomain", "statusText", "length", "Protection", "removeAllListeners", "map", "browser", "findChallengeScript", "isSearchEngine", "_asap", "onProtectionInitialized", "array", "referrer", "observe", "__esModule", "isPrototypeOf", "deleteCookie", "OPTIONS", "validate", "performance", "append", "reese84_performance", "toStringTag", "Headers", "port1", "ROTL", "getEntriesByType", "Chrome", "reeseSkipExpirationCheck", "bodyUsed", "_bodyInit", "nodeName", "undefined", "BonServer", "onload", "Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.", "push", "debug", "setCookie", "setTimeout", "timer", "setItem", "Recaptcha", "400", "stop", "HEAD", "clearTimeout has not been defined", "application/json; charset=utf-8", "__proto__", "filter", "interrogation", "cache_", "true", "You must pass an array to race.", "create", "substring", "run", "currentToken", "_bodyText", "[object Uint8ClampedArray]", "_bodyArrayBuffer", "toString", "createTextNode", "token", "responseText", "document", "DateTimer", "split", "loading", "700", "Request error for 'POST ", "currentTokenExpiry", "setTimeout has not been defined", "Sequentum", "retry", "duration", "then", "toUpperCase", "text/plain;charset=UTF-8", "_remaining", "forEach", "random", "; samesite=lax", "Network request failed", "tokenEncryptionKeySha2", "off", "appendQueryParam", "setPrototypeOf", "__awaiter", "nextTick", "prototype", "[object Uint8Array]", "apply", "type", "prependListener", "callback", "DOMContentLoaded", "total", "extractCookie", "toLowerCase", "_script_", "binding", "[object process]", "return", "RecoverableError", "PerformanceTimer", "triggerTimeMs", "all", "onProtectionLoaded", "listeners", "version", "could not read FormData body as text", "match", "audio", "[object Float64Array]", "promise", "_setScheduler", "Firefox", "could not read FormData body as blob", "solve", "join", "readyState", "RobustScheduler", "URLSearchParams", "constructor", "runOnLoop", "setToken", "data-advanced", "getSeconds", "default", "Win64", "emit", "You cannot resolve a promise with itself", "mark", "FileReader", "object", "summary", "callGlobalCallback", "stable error: ", "httpClient", "exports", "next", "text", "=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT", "_instanceConstructor", "Win32", "string", "mode", "process.chdir is not supported", "postMessage", "___dTL", "platform", "_bodyBlob", "[object Float32Array]", "done", "stopInternal", "_willSettleAt", "[object Promise]", "resolve", "media", "Invalid character in header field name", "status", "visibilitychange", "setRequestHeader", "waitingOnToken", "title", "fun", "getOwnPropertyNames", "port2", "cwd", "floor", "video", "MutationObserver", "tokenExpiryCheck", "Windows", "navigator", "getAllResponseHeaders", "solution", "location", "online", "fromTokenResponse", "?cachebuster=", "Linux", "startInternal", "value", "polyfill", "protectionSubmitCaptcha", "set", "_eachEntry", "__s", "(^| )", "getElementById", "__web", "_unwrapped", "_bodyFormData", "log", "json", "get", "/Criciousand-meth-shake-Exit-be-till-in-ches-Shad", "finally", "result", "sent", "name", "[object Uint32Array]", "Non-ok status code: ", "setSeconds", "readAsArrayBuffer", "started", "renewInSec", "initializeProtection", "enableFull", "pop", "ops", "cast", "reese84", "Invalid status code", "Mavericks", "removeListener", "=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=", "ArrayBuffer", "versions", "isArray", "slice", "cookie", "function", "screen", "addEventListener", "reduce", "update", "CaptchaPayload", "buffer", "body", "keys", "marks", "catch", "Protection has not started.", "; domain=", "interrogate", "measure", "unsupported BodyInit type", "tion", "throw", "redirect", "__fx", "COOKIE_NAME", "Array Methods must be provided an Array", "COOKIE_NAME_SECONDARY", "pageshow", "number", "scheduler", "external", "stack", "readAsText", "now", "reeseRetriedAutoload", "submitCaptcha", "_onerror", "omit", "_stop", "call", "Promise", "timerId", "_settledAt", "x-d-token", "GET", "iterator", "uate", "_state", "extractTokenLocalStorage", "updateToken", "X-Request-URL", "PRIMARY_COOKIE", "Body not allowed for GET or HEAD requests", "start", "SECONDARY_COOKIE", "runLater", "headers", "web", "__extends", "min", "stripQuery", "Yosemite", "prependOnceListener", "ontimeout", "=([^;]+)", "return this", "[object Int32Array]", "entries", "fetch", "currentTokenError", "src", "stable", "progress", "toHexStr"];

def substring_extract(file_name: str) -> None:
    substring_pattern = re.compile(r'(getFunctionString\(["|\'](0[xX][0-9a-fA-F]+)["|\']\))')

    #input file
    input_file = open(file_name, "rt")
    #output file to write the result to
    output_file = open("out.js", "wt")
    #for each line in the input file
    for line in input_file:
        substring_iterator = substring_pattern.finditer(line)
        for match in substring_iterator:
            if (len(match.groups()) != 2):
                print(len(match.groups()))
                continue

            string_index = int(match.group(2), 16)

            replacement = function_strings[string_index]
            replacement = replacement.replace("\"", "\\\"")
            line = line.replace(match.group(1), "\"" + replacement + "\"")
        
        output_file.write(line)

    #close input and output files
    input_file.close()
    output_file.close()

def main(argv : List[str]):
    if len(argv) > 1:
        return;
    substring_extract(argv[0])

if __name__ == "__main__":
    main(sys.argv[1:])